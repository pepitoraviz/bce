const Mongoose = require('mongoose');
Mongoose.connect(
  process.env.MONGO_URL || 'mongodb://localhost/bce-api',
  { useNewUrlParser: true }
);
const db = Mongoose.connection;
const log = console.log;
const logErr = console.error;

exports.init = () => {
  return new Promise((resolve, reject) => {
    db.on('connected', () => {
      log('Database connection stablished');
      resolve();
    });
    db.on('error', err => {
      logErr('ERR: ', err);
      reject(err);
    });
  });
};
