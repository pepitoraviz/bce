const UsersModel = require('./api/users/users.model');


const seedAdmin = async () => {
    let admin = await UsersModel.findOne({email: 'admin@bceapp.com'})
    if (admin) {
        admin.remove();
    }
    let adminModel = {
        email: 'admin@bceapp.com',
        firstName: 'Admin',
        lastName: 'Bceapp',
        role: 'ADMIN',
        password: 'password'
    }
    await UsersModel.remove({email: 'admin@bceapp.com'});
    await UsersModel.create(adminModel);
    console.log(" ==== DEFAULT ADMIN USER CREATED ======");

}

exports.seedAdmin = seedAdmin;