const log = console.log;
const logErr = console.error;
const Path = require('path');

exports.initRoutes = (server) => {
    return new Promise( async (resolve, reject) => {
        try {
            await server.route(require('./api/posts/index'));
            await server.route(require('./api/users/index'));
            await server.route(require('./api/applications/index'));
            await server.route(require('./api/schedule/index'));
            await server.route(require('./api/student_notif/index'));
            await server.route(require('./api/admin_notif/index'));
            await server.route(require('./api/comments/index'));
            await server.route(require('./api/settings/index'));
            await server.route({
                method:'GET',
                path:'/',
                handler:(req, h) => "BCE API"
            })
            await server.route({
                method: 'GET',
                path: '/uploads/{param*}',
                handler: {
                    directory: {
                        path: 'uploads'
                    }
                }
            });
            console.log('Successfully setup routes');
            return resolve();
        } catch (error) {
            logErr(error);
            reject(error);
        }
    })
}