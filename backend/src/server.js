
'use strict';

const Hapi = require('hapi');
const log = console.log;
const server = new Hapi.Server({
    port: process.env.PORT || '5000',
    routes: { cors: { origin: ['*']}},
    debug: { 'request': ['error', 'uncaught']}
})






module.exports = server;