const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const scheduleSchema = new Schema({
    userId: {
        required: true,
        type: Schema.Types.ObjectId,
        ref: 'Users'
    },
    applicationId: {
        type: Schema.Types.ObjectId,
        ref: 'Applications'
    },
    schedule: Date

}, { timestamps: true})


module.exports = mongoose.model('Schedules', scheduleSchema);
