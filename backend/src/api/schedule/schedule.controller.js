'use strict';

const SchedulesModel = require('./schedule.model');
const ApplicationModel = require('../applications/applications.model');
const Boom = require('boom');
const _ = require('lodash');


exports.create = async (req, h) => {
    try {
        const payload = req.payload;
        console.log('payload: ', payload);
        let checkIfSched = await SchedulesModel.findOne({
            applicationId: payload.applicationId
        });
        if (!checkIfSched) {
            const schedule = await SchedulesModel.create(payload);
            const application = await ApplicationModel.findById(payload.applicationId);
            if (application) {
                application.isScheduled = true;
                await application.save();
                return schedule;
            }
            return schedule;
        } else {
            payload.schedule = new Date(payload.schedule);
            checkIfSched = Object.assign(checkIfSched, payload);
            await checkIfSched.save();
            return checkIfSched;
        }
    } catch (error) {
        console.log('error: ', error);
        return Boom.badRequest('POST  - /api/schedule/create ', ' Unknown error happened')
    }
};
exports.getTodayOnly = async (req, h) => {
    console.log('wow')
    try {
        var baseDate = new Date();
        var currentDate = new Date(baseDate.setDate(baseDate.getDate() - 1));
        var nextDate = new Date(baseDate.setDate(baseDate.getDate() + 2));
        const scheds = await SchedulesModel.aggregate([
            {$match: {
                schedule: {
                    $gte: currentDate,
                    $lt: nextDate
                }
            }},
            {
                $lookup: {
                    from: 'applications',
                    localField: 'applicationId',
                    foreignField: '_id',
                    as: 'applicationId'
                }
            },
            {
                $unwind: '$applicationId'
            },
            {
                $match: {
                    'applicationId.isApproved': false,
                    'applicationId.isScheduled': true
                }
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'userId',
                    foreignField: '_id',
                    as: 'userId'
                }
            },
            {
                $unwind: '$userId'
            },
            {
                $lookup: {
                    from: 'requirements',
                    localField: 'applicationId.requirementId',
                    foreignField: '_id',
                    as: 'requirementId'
                }
            },
            {
                $unwind: '$requirementId'
            },
            {
                $sort: {
                    schedule: -1
                }
            },
            {
                $project: {
                    _id: "$_id",
                    userId: {
                        _id: "$userId._id",
                        firstName: '$userId.firstName',
                        lastName: '$userId.lastName',
                        barangay: '$userId.barangay',
                        contactNumber: '$userId.contactNumber'
                    },
                    applicationId: {
                        _id: '$applicationId._id',
                        requirementId: '$requirementId',
                        yearLvl: '$applicationId.yearLvl',
                        applicationType: '$applicationId.applicationType'
                    },
                    schedule: "$schedule"
                }
            }
        ])
        return scheds;
        return 'haha';
    } catch (error) {
        console.log('error: ', error);
        return Boom.badRequest('GET  - /api/schedule/todate ', ' Unknown error happened')
    }
}
exports.getAll = async (req, h) => {
    try {
        const scheds = await SchedulesModel.aggregate([{
            $lookup: {
                    from: 'applications',
                    localField: 'applicationId',
                    foreignField: '_id',
                    as: 'applicationId'
                }
            },
            {
                $unwind: '$applicationId'
            },
            {
                $match: {
                    'applicationId.isApproved': false,
                    'applicationId.isScheduled': true
                }
            },
            {
                $lookup: {
                    from: 'users',
                    localField: 'userId',
                    foreignField: '_id',
                    as: 'userId'
                }
            },
            {
                $unwind: '$userId'
            },
            {
                $lookup: {
                    from: 'requirements',
                    localField: 'applicationId.requirementId',
                    foreignField: '_id',
                    as: 'requirementId'
                }
            },
            {
                $unwind: '$requirementId'
            },
            {
                $sort: {
                    schedule: -1
                }
            },
            {
                $project: {
                    _id: "$_id",
                    userId: {
                        _id: "$userId._id",
                        firstName: '$userId.firstName',
                        lastName: '$userId.lastName',
                        barangay: '$userId.barangay',
                        contactNumber: '$userId.contactNumber'
                    },
                    applicationId: {
                        _id: '$applicationId._id',
                        requirementId: '$requirementId',
                        yearLvl: '$applicationId.yearLvl',
                        applicationType: '$applicationId.applicationType'
                    },
                    schedule: "$schedule"
                }
            }
        ])
        return scheds;
    } catch (error) {
        return [];
    }
}