const controller = require('./schedule.controller');



const routes = [{
    method:'POST',
    path:'/api/schedules',
    handler:controller.create
}, {
    method: 'GET',
    path: '/api/schedules/todate',
    handler: controller.getTodayOnly
},{
    method: 'GET',
    path: '/api/schedules',
    handler: controller.getAll
}]


module.exports = routes;