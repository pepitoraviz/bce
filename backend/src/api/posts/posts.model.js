const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const postSchema = new Schema({
    title: {
        require: true,
        type: String
    },
    content: {
        type: String,
        required: true
    },
    createdBy: Schema.Types.ObjectId,
    isArchived: {
        type: Boolean,
        default: () => false
    }
}, { timestamps: true})


module.exports = mongoose.model('Posts', postSchema);
