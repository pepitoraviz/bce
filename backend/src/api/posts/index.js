const controller = require('./posts.controller');



const routes = [{
    method:'POST',
    path:'/api/posts',
    handler:controller.create
},{
    method:'GET',
    path:'/api/posts',
    handler:controller.getAllPosts
},{
    method:'GET',
    path:'/api/posts/{postId}',
    handler:controller.getPostById
},{
    method:'PUT',
    path:'/api/posts/{postId}',
    handler:controller.edit
},{
    method:'PATCH',
    path:'/api/posts/{postId}',
    handler:controller.edit
},{
    method:'DELETE',
    path:'/api/posts/{postId}',
    handler:controller.archivePost
}, {
    method: 'GET',
    path: '/api/posts/getByQuery',
    handler:controller.getByQuery
}]


module.exports = routes;