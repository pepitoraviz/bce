'use strict';

const PostsModel = require('./posts.model');
const Boom = require('boom');
const _ = require('lodash');


exports.create = async (req, h) => {

    try {
        const payload = req.payload;
        let post = await PostsModel.create(payload);
        return post;
    } catch (error) {
        console.log('error: ', error);
        return Boom.badRequest('POST  - /api/create ',' Unknown error happened')
    }
};


exports.edit = async (req, h) => {

    try {
        const params = req.params;
        let payload = req.payload;
        if ('undefined' !== typeof payload._id ) {
            delete payload._id
        }
        let tempPost = await PostsModel.findById(params.postId);
        if('undefined' !== typeof tempPost) {
             tempPost = _.merge(tempPost, payload);
            await tempPost.save();
            return tempPost;
        }
        return h.response('Post not found').code(404);
    } catch (error) {
        console.log('error: ', error);
        return Boom.badRequest('POST  - /api/edit ',' Unknown error happened')
    }
};


exports.archivePost = async (req, h) => {
    try {
        const params = req.params;
        let payload = req.payload;
        if ('undefined' !== typeof payload._id ) {
            delete payload._id
        }
        let tempPost = await PostsModel.findById(params.postId);
        if('undefined' !== typeof tempPost) {
            tempPost.isArchived = true;
            await tempPost.save();
            return tempPost;
        }
        return h.response('Post not found').code(404);
    } catch (error) {
          console.log('error: ', error);
        return Boom.badRequest('POST  - /api/archive ',' Unknown error happened')
    }
}

exports.getAllPosts = async(req, h) =>{
    try {
        let posts = await PostsModel.find({isArchived:false}).sort('-createdAt')
        return posts;
    } catch (error) {
          console.log('error: ', error);
        return Boom.badRequest('POST  - /api/archive ',' Unknown error happened')
    }
}

exports.getPostById = async(req, h) => {
     try {
        const params = req.params;
        let posts = await PostsModel.findById(params.postId);
        return posts;
    } catch (error) {
        console.log('error: ', error);
        return Boom.badRequest('POST  - /api/archive ',' Unknown error happened')
    }
}

exports.getByQuery = async(req, h) => {
    try {
        const query = req.query;
        let searchQuery = {};

        if('undefined' !== typeof query.postTitle) {
            searchQuery['title'] = { $regex: query.postTitle, $options: 'i'}
        }

         if('undefined' !== typeof query.postDate) {
            var baseDate = new Date(query.postDate);
            var currentDate = new Date(baseDate.setDate(baseDate.getDate() -1 ));
            var nextDate = new Date(baseDate.setDate(baseDate.getDate() + 2 ));
            searchQuery['createdAt'] = { $gt: currentDate, $lt: nextDate}
        }
        const posts = await PostsModel.find(searchQuery);
        return posts;
    } catch (error) {
        console.log('error: ', error);
        return Boom.badRequest('GET  - /api/getByQuery ',' Unknown error happened')
    }
}
