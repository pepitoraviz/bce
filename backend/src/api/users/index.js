const controller = require('./users.controller');



const routes = [{
    method: 'GET',
    path: '/api/users/scholars',
    handler: controller.getAllScholars
},{
    method:'POST',
    path:'/api/users/register',
    handler:controller.register
},{
    method:'POST',
    path:'/api/users/login',
    handler:controller.login
},{
    method:'GET',
    path:'/api/users/{userId}',
    handler:controller.getProfile
},{
    method:'PUT',
    path:'/api/users/{userId}',
    handler:controller.updateUser
},{
    method:'PATCH',
    path:'/api/users/{userId}',
    handler:controller.updateUser
}, {
    method:'PUT',
    path:'/api/users/{userId}/suspend',
    handler:controller.suspend
},  {
    method:'PUT',
    path:'/api/users/{userId}/unsuspend',
    handler:controller.unsuspend
}, {
    method:'PUT',
    path:'/api/users/{userId}/confirm',
    handler:controller.confirm
}, {
    method: 'GET',
    path: '/api/users/barangay_report',
    handler: controller.getPerBarangayReport
}];


module.exports = routes;