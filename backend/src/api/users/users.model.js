const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const usersSchema = new Schema({
    email: {
        require: true,
        type: String,
        unique: 'Email already in used'
    },
    role: {
        type: String,
        enum: ["SCHOLAR", "ADMIN", "STAFF"]
    },
    password: {
        type: String,
        required: true
    },
    isSuspended: { 
        type: Boolean,
        default: false
    },
    contactNumber: String,
    suffix: String,
    firstName: String,
    lastName: String,
    barangay: String,
    address: String,
    schoolName: String,
    schoolAddress: String,
    isConfirmed: {
        type: Boolean,
        default: false,
    },
    middleName: String,
    validity: Date,
    moFirstName: String,
    moLastName: String,
    moMiddleName: String,
    moAge: String,
    moContact: String,
    moOccupation: String,
    moBarangay: String,
    moAddress: String,
    faFirstName: String,
    faLastName: String,
    faMiddleName: String,
    faSuffix: String,
    faAge: String,
    faContact: String,
    faOccupation: String,
    faBarangay: String,
    faAddress: String,
    birthCerth: String
}, {
    timestamps: true
})

usersSchema.methods = {
    view() {
        return {
            _id: this._id,
            email: this.email,
            suffix: this.suffix,
            role: this.role,
            firstName: this.firstName,
            lastName: this.lastName,
            barangay: this.barangay,
            address: this.address,
            contactNumber: this.contactNumber,
            schoolName: this.schoolName,
            schoolAddress: this.schoolAddress,
            isConfirmed: this.isConfirmed,
            moFirstName: this.moFirstName,
            moLastName: this.moLastName,
            middleName: this.middleName,
            moMiddleName: this.moMiddleName,
            moAge: this.moAge,
            moContact: this.moContact,
            moOccupation: this.moOccupation,
            moBarangay: this.moBarangay,
            moAddress: this.moAddress,
            faFirstName: this.faFirstName,
            faLastName: this.faLastName,
            faMiddleName: this.faMiddleName,
            faAge: this.faAge,
            faContact: this.faContact,
            faOccupation: this.faOccupation,
            faBarangay: this.faBarangay,
            faAddress: this.faAddress,
            birthCerth: this.birthCerth,
            isSuspended: this.isSuspended
        }
    }
}

module.exports = mongoose.model('Users', usersSchema);