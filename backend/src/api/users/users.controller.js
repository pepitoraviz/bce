'use strict';

const UsersModel = require('./users.model');
const Boom = require('boom');
const _ = require('lodash');
const Evnts = require('../../events');
const AdminNotifController = require('../admin_notif/admin_notif.controller');
const StudentNotifCtrl = require('../student_notif/student_notif.controller');
exports.getAllScholars = async (req, h) => {
    try {
        const scholars = await UsersModel.find({
            role: 'SCHOLAR'
        });
        let formatedVal = []
        if (scholars && Array.isArray(scholars) && scholars.length > 0) {
            scholars.forEach(async (scholar) => {
                formatedVal.push(await scholar.view());
            });
            return formatedVal;
        }

        return scholars;
    } catch (error) {
        return Boom.badData('Unknown error happened while fetching scholar list');
    }
}
exports.register = async (req, h) => {
    try {
        const payload = req.payload;
        let register = await UsersModel.create(payload);
        return register.view();
    } catch (error) {
        console.log('error: ', error);
        return Boom.badRequest('POST  - /api/users/register ', ' Unknown error happened')
    }
}


exports.getProfile = async (req, h) => {
    try {
        const params = req.params;
        let user = await UsersModel.findById(params.userId);

        if (user) {
            return user.view();
        }
        return h.response({
            message: 'User not found'
        }).code(404);
    } catch (error) {
        console.log('error: ', error);
        return Boom.badRequest('GET  - /api/users/{userId}getProfile ', ' Unknown error happened')
    }
}

exports.login = async (req, h) => {
    try {
        const payload = req.payload;
        let user = await UsersModel.findOne({
            email: payload.email
        });
        if (user) {
            if (payload.password === user.password) {
                return user.view()
            }
            return {
                error: 'invalid email or password '
            }
        }


        return h.response({
            message: 'No account is associated with that email'
        }).code(404);

    } catch (error) {
        console.log('error: ', error);
        return Boom.badRequest('GET  - /api/login/', ' Unknown error happened')
    }
}


exports.updateUser = async (req, h) => {
    try {
        const params = req.params;
        let payload = req.payload;
        if ('undefined' !== typeof payload._id) {
            delete payload._id
        }
        let tempUser = await UsersModel.findById(params.userId);

        if ('undefined' !== typeof tempUser) {
            tempUser = _.merge(tempUser, payload);
            await tempUser.save();
            await AdminNotifController.createNotif({
                userId: tempUser._id,
                title: 'Profile Update',
                content: `${tempUser.firstName} ${tempUser.lastName} updated his / her profile`
            });
            Evnts.emitEvent('scholar_profile:updated', {
                scholarName: `${tempUser.firstName} ${tempUser.lastName}`
            });
            return tempUser.view();
        }
        return h.response('User not found').code(404);
    } catch (error) {
        console.log('error: ', error);
        return Boom.badRequest('GET  - /api/users/{userId}updateUser ', ' Unknown error happened')
    }
}

exports.suspend = async (req, h) => {
    try {
        const params = req.params;
        let payload = req.payload;
        if ('undefined' !== typeof payload._id) {
            delete payload._id
        }
        let tempUser = await UsersModel.findById(params.userId);

        if ('undefined' !== typeof tempUser) {
            tempUser = _.merge(tempUser, payload);
            await tempUser.save();
            await StudentNotifCtrl.createNotif({
                userId: tempUser._id,
                title: 'Account Suspened',
                content: `Your account has been suspended please report to BCE office ASAP`
            });
            Evnts.emitEvent('account:suspended', {
                userId: tempUser._id
            });
            return tempUser.view();
        }
        return h.response('User not found').code(404);
    } catch (error) {
        console.log('error: ', error);
        return Boom.badRequest('GET  - /api/users/{userId}updateUser ', ' Unknown error happened')
    }
}

exports.confirm = async (req, h) => {
    try {
        const params = req.params;
        let payload = req.payload;
        if ('undefined' !== typeof payload._id) {
            delete payload._id
        }
        let tempUser = await UsersModel.findById(params.userId);

        if ('undefined' !== typeof tempUser) {
            tempUser = _.merge(tempUser, payload);
            await tempUser.save();
            await StudentNotifCtrl.createNotif({
                userId: tempUser._id,
                title: 'Account Confirmed',
                content: `Your account has been confirmed`
            });
            Evnts.emitEvent('account:confirmed', {
                userId: tempUser._id
            });
            return tempUser.view();
        }
        return h.response('User not found').code(404);
    } catch (error) {
        console.log('error: ', error);
        return Boom.badRequest('GET  - /api/users/{userId}updateUser ', ' Unknown error happened')
    }
}
exports.unsuspend = async (req, h) => {
    try {
        const params = req.params;
        let payload = req.payload;
        if ('undefined' !== typeof payload._id) {
            delete payload._id
        }
        let tempUser = await UsersModel.findById(params.userId);

        if ('undefined' !== typeof tempUser) {
            tempUser = _.merge(tempUser, payload);
            await tempUser.save();
            await StudentNotifCtrl.createNotif({
                userId: tempUser._id,
                title: 'Account Unsuspended',
                content: `Your account has been successfully unsuspened`
            });
            Evnts.emitEvent('account:unsuspend', {
                userId: tempUser._id
            });
            return tempUser.view();
        }
        return h.response('User not found').code(404);
    } catch (error) {
        console.log('error: ', error);
        return Boom.badRequest('GET  - /api/users/{userId}updateUser ', ' Unknown error happened')
    }
}


exports.getPerBarangayReport = async () => {
    try {

        const bagongIlog = await UsersModel.find({
            barangay: 'bagong_ilog'
        })
        const bagongKatipunan = await UsersModel.find({
            barangay: 'bagong_katipunan'
        })
        const buting = await UsersModel.find({
            barangay: 'buting'
        })
        const bambang = await UsersModel.find({
            barangay: 'bambang'
        })
        const caniogan = await UsersModel.find({
            barangay: 'caniogan'
        })
        const kapitolyo = await UsersModel.find({
            barangay: 'kapitolyo'
        })
        const kapasigan = await UsersModel.find({
            barangay: 'kapasigan'
        })
        const malinao = await UsersModel.find({
            barangay: 'malinao'
        })
        const oranbo = await UsersModel.find({
            barangay: 'oranbo'
        })
        const palatiw = await UsersModel.find({
            barangay: 'palatiw'
        })
        const pineda = await UsersModel.find({
            barangay: 'pineda'
        })
        const sagad = await UsersModel.find({
            barangay: 'sagad'
        })
        const sanAntonio = await UsersModel.find({
            barangay: 'san_antonio'
        })
        const sanJoaquin = await UsersModel.find({
            barangay: 'san_joaquin'
        })
        const sanJose = await UsersModel.find({
            barangay: 'san_jose'
        })
        const sanNicolas = await UsersModel.find({
            barangay: 'san_nicolas'
        })
        const staCruz = await UsersModel.find({
            barangay: 'sta_cruz'
        });
        const staRosa = await UsersModel.find({
            barangay: 'sta_rosa'
        });
        const stoTomas = await UsersModel.find({
            barangay: 'sto_tomas'
        });
        const sumilang = await UsersModel.find({
            barangay: 'sumilang'
        });
        const ugong = await UsersModel.find({
            barangay: 'ugong'
        });
        const pinagBuhatan = await UsersModel.find({
            barangay: 'pinag_buhatan'
        });
        await Promise.all([bagongIlog,
            bagongKatipunan,
            buting,
            bambang,
            caniogan,
            kapitolyo,
            kapasigan,
            oranbo,
            malinao,
            palatiw,
            pineda,
            sanAntonio,
            sanJoaquin,
            sanJose,
            sanNicolas,
            staCruz,
            staRosa,
            stoTomas,
            sumilang,
            ugong,
            pinagBuhatan
        ]);
        return [{
            text: 'Bagong Ilog',
            val: bagongIlog.length
        }, {
            text: 'Pinag Buhatan',
            val: pinagBuhatan.length
        }, {
            text: 'Bagong Katipunan',
            val: bagongKatipunan.length
        }, {
            text: 'Bambang',
            val: bambang.length
        }, {
            text: 'Buting',
            val: buting.length
        }, {
            text: 'Caniogan',
            val: caniogan.length
        }, {
            text: 'Kapitolyo',
            val: kapitolyo.length
        }, {
            text: 'Kapasigan',
            val: kapasigan.length
        }, {
            text: 'Malinao',
            val: malinao.length
        }, {
            text: 'Oranbo',
            val: oranbo.length
        }, {
            text: 'Palatiw',
            val: palatiw.length
        }, {
            text: 'Pineda',
            val: pineda.length
        }, {
            text: 'Sagad',
            val: sagad.length
        }, {
            text: 'San Antonio',
            val: sanAntonio.length
        }, {
            text: 'San Joaquin',
            val: sanJoaquin.length
        }, {
            text: 'San Jose',
            val: sanJose.length
        }, {
            text: 'San Nicolas',
            val: sanNicolas.length
        }, {
            text: 'Sta. Cruz',
            val: staCruz.length
        }, {
            text: 'Sta. Rosa',
            val: staRosa.length
        }, {
            text: 'Sto. Tomas',
            val: stoTomas.length
        }, {
            text: 'Sumilang',
            val: sumilang.length
        }, {
            text: 'Ugong',
            val: ugong.length
        }, ]
    } catch (error) {
        console.log('error: ', error);

    }
}