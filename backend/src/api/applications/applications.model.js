const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const applicationSchema = new Schema({
    userId: {
        required: true,
        type: Schema.Types.ObjectId,
        ref: 'Users'
    },
    genAve: {
        type: Number,
        required: true,
        default: 0
    },
    requirementId: {
        type: Schema.Types.ObjectId,
        ref: 'Requirements'
    },
    schoolYear: {
        type: String,
        required: true
    },
    semester: {
        type: String,
        required: true
    },
    isApproved: {
        type: Boolean,
        default: false
    },
    isScheduled: {
        type: Boolean,
        default: false
    },
    yearLvl: {
        type: String,
        enum: ["PRIM", "SEC", "TERT"],
        default: "PRIM"
    },
    applicationType: {
        type: String,
        enum: [ "ACAD", "SPORT", "OSY" ]
    }
}, { timestamps: true})


applicationSchema.methods = {
    view() {
        return this.populate('userId requirementId').execPopulate();
    }
}

applicationSchema.statics = {
    getPendings() {
        return this.find({ isApproved: false }).populate('userId requirementId');
    }
}

module.exports = mongoose.model('Applications', applicationSchema);
