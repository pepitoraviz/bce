'use strict';

// const UsersModel = require('./users.model');
const Boom = require('boom');
const _ = require('lodash');
const fs = require('fs');
const UPLOAD_PATH = 'uploads';

const fileOptions = {
    dest: `${UPLOAD_PATH}/`
};
const uuid = require('uuid');
const ApplicationModel = require('./applications.model');
const RequirementModel = require('../requirements/requirements.model');
const StudentController = require('../student_notif/student_notif.controller');
const Evnts = require('../../events');
const moment = require('moment');
exports.getAllPendingApps = async () => {
    try {
        const pendingApps = await ApplicationModel.getPendings();
        return pendingApps;
    } catch (error) {
        return h.response('Unknown error happened').code(500);
    }
}

exports.getAllApplicationByUser = async (req, h) => {
    try {
        const params = req.params;
        const applications = await ApplicationModel.find({
            userId: params.userId
        });
        let populatedApps = [];
        if (applications) {
            // return await applications.view();
            populatedApps = applications.map(async (app) => {
                return await app.view();

            })
            return await Promise.all(populatedApps);
        }
        return h.response('No applications found').code(404);
    } catch (error) {
        console.log('error: ', error);
        return h.response('Unknown error happened').code(500);
    }
}
exports.submitApplication = async (req, h) => {
    let payload = req.payload;
    const confFile = '../../config.json';
    let config = require(confFile);
    payload.schoolYear = config['schoolYear'];
    payload.semester = config['semester']
    let application = await ApplicationModel.create(payload);
    let requirementPayload = {
        userId: payload.userId,
    };
    let requirement = await RequirementModel.create(requirementPayload);
    application.requirementId = requirement._id;
    await application.save();
    return application;
}
exports.uploadApplication = async (req, h) => {
    const payload = req.payload;
    const params = req.params;
    const file = payload['requirement'];
    const fileDetails = await upload(file, fileOptions);
    let requirement = await RequirementModel.findById(payload.reqId);
    if (requirement) {
        requirement[params.type] = fileDetails.path


        await requirement.save();
        return requirement
    }
    return h.response('Requirement not found').code(404);
}



function upload(file, opts) {
    if (!file) throw new Error('no file(s)');

    return _fileHandler(file, opts);
}

function _fileHandler(file, opts) {
    if (!file) throw new Error('no file');

    const originalName = file.hapi.filename;
    const fileName = uuid.v1();
    const path = `${opts.dest}${fileName}.pdf`;
    const fileStream = fs.createWriteStream(path);

    return new Promise((resolve, reject) => {
        file.on('err', (err) => {
            reject('err');
        })

        file.pipe(fileStream);

        file.on('end', () => {
            const fileDetails = {
                fieldname: file.hapi.name,
                originalname: file.hapi.filename,
                fileName,
                destination: `${opts.dest}`,
                path,
                size: fs.statSync(path).size,
            }
            resolve(fileDetails);
        })
    })
}

exports.approveApplication = async (req, h) => {
    try {
        const payload = req.payload;
        const application = await ApplicationModel.findOneAndUpdate({
            _id: payload.appId
        }, {
            isApproved: true
        }, {
            new: true
        });
        if (application) {
            await StudentController.createNotif({
                userId: application.userId,
                title: 'Scholarship Approval',
                content: `Your application for ${getScholarshipType(application.applicationType)} scholarship is granted !`
            });
            Evnts.emitEvent('application:approved', {
                userId: application.userId
            });
            return {
                message: 'App succesfully approved',
                approved: true
            }
        }
        return h.response('Application not found').code(404);
    } catch (error) {
        return h.response('Application not found').code(404);
    }
}

function getScholarshipType(type) {
    if ("ACAD" === type) {
        return 'Academic'
    }
    if ('SPORT' === type) {
        return 'Sports'
    }
    if ('OSY' === type) {
        return 'Osy'
    }
    return 'Academic';
}


exports.getAllAppThisMonth = async (req, h) => {
    const baseDates = getFirstAndLastDayOfMonth();
    const dates = generateDates(baseDates.firstDay, baseDates.lastDay)
    let dateResults = dates.map(async function(d) {
        const result  = await getAllApplicationByDate(d) ;
        const resultObj =  { date: d, result: result }
        return resultObj;
    });
    const res = await Promise.all(dateResults);
    return res;
};
 function generateDates(startDate, endDate) {
    var dateArray = [];
    var currentDate = moment(startDate);
    var stopDate = moment(endDate);
    while (currentDate <= stopDate) {
        dateArray.push(moment(currentDate).format('YYYY-MM-DD'))
        currentDate = moment(currentDate).add(1, 'days');
    }
    return dateArray;
}

function getFirstAndLastDayOfMonth() {
    var date = new Date(),
        y = date.getFullYear(),
        m = date.getMonth();
    var firstDay = new Date(y, m, 1);
    var lastDay = new Date(y, m + 1, 0);
    return {
        firstDay,
        lastDay
    }
}
async function getAllApplicationByDate(date) {
    return new Promise(async (resolve, reject) => {
        if (date) {
            var baseDate = new Date(date);
            var currentDate = new Date(baseDate.setDate(baseDate.getDate() - 1));
            var nextDate = new Date(baseDate.setDate(baseDate.getDate() + 2));
            const application = await ApplicationModel.find({
                createdAt: {
                    $gt: currentDate,
                    $lt: nextDate
                }
            })
            return resolve(application.length)
        }
        return resolve(0)
    })
}


exports.getRankingByTypeAndYearLvl = async (req, h) => {
    try {
        const params = req.params;
        const applications = ApplicationModel.aggregate([
            {$match:params},
            {$lookup: {from: 'users', localField: 'userId', foreignField: '_id', as: 'user'}},
            {$unwind:'$user'},
            {$match: { 'user.isSuspended': false}},
            {$limit: 10},
            {$project:{
                user:{
                    password: 0
                }
            }}
        ]);
        return applications
    } catch (error) {
        console.log('error: ', error);
        return h.response('Unknown error happened').code(500);
    }
}

exports.getApplicationCountByType = async (req, h) => {
    try {
        const params = req.params;
        const applications = await ApplicationModel.find(params);
        return applications.length;
    } catch (error) {
        console.log('error: ', error);
        return h.response('Unknown error happened').code(500);
    }
}


exports.getApplicationBreakDown = async (req, h) => {
    try {
        const primAcad =  await ApplicationModel.find({applicationType:"ACAD", yearLvl:"PRIM"});
        const secAcad =  await ApplicationModel.find({applicationType:"ACAD", yearLvl:"SEC"});  
        const tertAcad =  await ApplicationModel.find({applicationType:"ACAD", yearLvl:"TERT"});
        const primSport =  await ApplicationModel.find({applicationType:"SPORT", yearLvl:"PRIM"});
        const secSport =  await ApplicationModel.find({applicationType:"SPORT", yearLvl:"SEC"});
        const tertSport =  await ApplicationModel.find({applicationType:"SPORT", yearLvl:"TERT"});
        const primOSY =  await ApplicationModel.find({applicationType:"OSY", yearLvl:"PRIM"});
        const secOSY = await ApplicationModel.find({applicationType:"OSY", yearLvl:"SEC"});
        const tertOSY = await  ApplicationModel.find({applicationType:"OSY", yearLvl:"TERT"});
        const total = await  ApplicationModel.find({});
        await Promise.all([primAcad, secAcad])
        const results = { primAcad, secAcad, tertAcad, primSport, secSport, tertSport, primOSY, secOSY, tertOSY} ;
        Object.keys(results).forEach((key) => {
            results[key] = results[key].length
        });
        return results;
    } catch (error) {
        console.log('error: ', error);
        return h.response('Unknown error happened').code(500);
    }
}

exports.getApplicationBreakDownTopLevel = async (req, h) => {
    try {
        const acad = await ApplicationModel.find({applicationType:"ACAD"});
        const sport = await ApplicationModel.find({applicationType:"SPORT"});
        const osy = await ApplicationModel.find({applicationType:"OSY"});
        await Promise.all([acad, sport, osy]);
        const results = { acad, sport, osy };
         Object.keys(results).forEach((key) => {
            results[key] = results[key].length
        });
        return results;
    } catch (error) {
        console.log('error: ', error);
        return h.response('Unknown error happened').code(500);
    }
}