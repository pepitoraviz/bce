const controller = require('./applications.controller');



const routes = [{
    method: 'GET',
    path: '/api/application/{userId}/getAll',
    handler: controller.getAllApplicationByUser
}, {
    method:'POST',
    path:'/api/application/submit',
    config:{
        handler: controller.submitApplication
    }
}, {
    method: 'POST',
    path: '/api/application/{type}/requirement',
    config:{
        handler: controller.uploadApplication,
        payload: {
            output: 'stream',
            allow: 'multipart/form-data' // important
        }
    }
}, {
    method: 'GET',
    path: '/api/applications',
    config: {
        handler : controller.getAllPendingApps
    }
}, {
    method: 'POST',
    path: '/api/applications/approve',
    handler: controller.approveApplication
}, {
    method: 'GET',
    path: '/api/applications/month/report',
    config: {
        handler: controller.getAllAppThisMonth
    }
}, {
    method: 'GET',
    path: '/api/applications/{applicationType}/{yearLvl}/getrank',
    handler: controller.getRankingByTypeAndYearLvl
}, {
    method: 'GET',
    path: '/api/applications/breakdown',
    handler: controller.getApplicationBreakDown
}, {
    method: 'GET',
    path: '/api/applications/breakdown_toplvl',
    handler: controller.getApplicationBreakDownTopLevel
}]

module.exports = routes;