'use strict';
const confFile = '../../config.json';
const fs = require('fs');
let config = require(confFile);


exports.getConfig = async (req, h) => {
    try {
        return config;
    } catch (error) {
         return h.response('Unknown error occured while getting the settings').code(500);
    }
}

exports.updateConfig = async (req, h) => {
    try {
        const payload = req.payload;
        const tempPayload = Object.assign(config, payload);
        fs.writeFile(confFile, JSON.stringify(tempPayload), function(err) {
            if (err){
                console.log(err);
                return h.response('Unknown error occured while updating the settings').code(500);
            }
        });
        return tempPayload;
    } catch (error) {
        console.log('error: ', error);
         return h.response('Unknown error occured while updating the settings').code(500);
    }
}