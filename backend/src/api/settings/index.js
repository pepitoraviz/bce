


const controller = require('./settings.controller');



const routes = [{
    method: 'GET',
    path:'/api/settings',
    handler:controller.getConfig
}, {
    method: 'PUT',
    path: '/api/settings',
    handler: controller.updateConfig
}]


module.exports = routes;