const controller = require('./student_notif.controller');



const routes = [{
    method: 'GET',
    path:'/api/student_notifs/{userId}',
    handler:controller.getMyNotifs
},{
    method: 'GET',
    path:'/api/student_notifs/{userId}/unread',
    handler:controller.getMyUnreadNotiifs
},{
    method: 'GET',
    path:'/api/student_notifs/{userId}/readall',
    handler:controller.readAllNotifByUser
}]


module.exports = routes;