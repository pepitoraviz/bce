const StudentNotifModel = require('./student_notif.model');


exports.createNotif = createNotif



async function createNotif(notifPayload) {
   return new Promise( async (resolve, reject) => {
       try {
            const notif = await StudentNotifModel.create(notifPayload);
            resolve(notif);
       } catch (error) {
           reject(error)
       }
   })
}

exports.readNotif = async (req, h) => {
    const params = req.params
    const notif = StudentNotifModel.findById(params.notifId);
    if (notif) {
        notif.isSeen = true;
        await notif.save();
        return notif;
    }
}
exports.getMyNotifs = async (req, h) => {
    const params = req.params;

    const myNotifs = await StudentNotifModel.find({ userId: params.userId }).sort('-createdAt');
    return myNotifs;
}

exports.getMyUnreadNotiifs = async (req, h) =>{
    const params = req.params;
    const myNotifs = await StudentNotifModel.find({ userId: params.userId, isSeen: false })
    return myNotifs;
}
exports.readAllNotifByUser = async (req, h)  => {
    const params = req.params;
    const notifs = await StudentNotifModel.update({ userId: params.userId}, { isSeen: true }, {new: true, multi: true});
    console.log('notifs: ', notifs);
    return { message: 'All notifs updated '}
}