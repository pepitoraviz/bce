const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const studentNotifSchema = new Schema({
    userId: {
        require: true,
        type: Schema.Types.ObjectId,
        ref: 'Users'
    },
    title: String,
    content: {
        type: String,
        required: true
    },
    isSeen: {
        type: Boolean,
        default: false
    }
}, { timestamps: true})


module.exports = mongoose.model('StudentNotifs', studentNotifSchema);
