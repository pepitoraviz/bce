const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const requirementsSchema = new Schema({
    userId: {
        required: true,
        type: Schema.Types.ObjectId,
        ref: 'Users'
    },
    birthCert: {
        type: String
    },
    schoolId: String,
    barangayCertificate: String,
    votersId: String,
    parentCedula: String,
    formOneThreeSeven: String,
    enrollmentSlip: String,
    currentRegForm: String,
    prevRegForm: String,
    studyLoad: String,
    enrollmentOr: String,
    prevSemCard: String,
    accomplishments: String,
    attendanceSportsClinic: String,
    coachEndorsmentLetter: String,
    guardianId: String,
    essay: String
}, { timestamps: true})


module.exports = mongoose.model('Requirements', requirementsSchema);
