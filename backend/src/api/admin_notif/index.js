const controller = require('./admin_notif.controller');



const routes = [{
    method: 'GET',
    path:'/api/admin_notif',
    handler:controller.getMyNotifs
},{
    method: 'GET',
    path:'/api/admin_notif/unread',
    handler:controller.getMyUnreadNotiifs
},{
    method: 'GET',
    path:'/api/admin_notif/readall',
    handler:controller.readAllNotif
}]


module.exports = routes;