const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const adminNotifSchema = new Schema({
    userId: {
        require: true,
        type: String
    },
    title: String,
    content: {
        type: String,
        required: true
    },
    isSeen: {
        type: Boolean,
        default: false
    }
}, { timestamps: true})


module.exports = mongoose.model('AdminNotifs', adminNotifSchema);
