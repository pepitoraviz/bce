const AdminNotifModel = require('./admin_notif.model');


exports.createNotif = createNotif



async function createNotif(notifPayload) {
   return new Promise( async (resolve, reject) => {
       try {
            const notif = await AdminNotifModel.create(notifPayload);
            resolve(notif);
       } catch (error) {
           reject(error)
       }
   })
}

exports.getMyNotifs = async (req, h) => {
    const params = req.params;
    const myNotifs = await AdminNotifModel.find().sort('-createdAt');
    return myNotifs;
}

exports.getMyUnreadNotiifs = async (req, h) =>{
    const params = req.params;
    const myNotifs = await AdminNotifModel.find({ isSeen: false })
    return myNotifs;
}
exports.readAllNotif = async (req, h)  => {
    const params = req.params;
    const notifs = await AdminNotifModel.update({}, { isSeen: true }, {new: true, multi: true});
    return { message: 'All notifs updated '}
}