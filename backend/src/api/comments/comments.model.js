const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const commnetsSchema = new Schema({
    userId: {
        require: true,
        type: Schema.Types.ObjectId,
        ref: 'Users'
    },
    comment: {
        type: String,
        required: true
    },
    postId: {
        required: true,
        type: Schema.Types.ObjectId,
        ref: 'Posts'
    }
}, { timestamps: true})


module.exports = mongoose.model('comments', commnetsSchema);
