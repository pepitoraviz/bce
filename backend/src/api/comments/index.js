const controller = require('./comments.controller');



const routes = [{
    method: 'GET',
    path:'/api/comments/{postId}',
    handler:controller.getPostComments
},{
    method: 'POST',
    path:'/api/comments',
    handler:controller.createComment
}]


module.exports = routes;