const CommentsModel = require('./comments.model');


exports.createComment = async (req, h) => {
       try {
            const payload = req.payload;
            const comment = await CommentsModel.create(payload);
            return comment
       } catch (error) {
           console.log('error: ', error);
        //    reject(error)
        return h.response('Unknown error happened').code(500);
       }
}
exports.getPostComments = async (req, h) => {
    const params = req.params;
    const comments = await CommentsModel.find({ postId: params.postId }).sort('-createdAt').populate('userId ')
    return comments;
}

// exports.getMyUnreadNotiifs = async (req, h) =>{
//     const params = req.params;
//     const myNotifs = await AdminNotifModel.find({ isSeen: false })
//     return myNotifs;
// }
// exports.readAllNotif = async (req, h)  => {
//     const params = req.params;
//     const notifs = await AdminNotifModel.update({}, { isSeen: true }, {new: true, multi: true});
//     return { message: 'All notifs updated '}
// }