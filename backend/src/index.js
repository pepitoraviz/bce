const Server = require('./server');
const logErr = console.error;
const log = console.log;
const Database = require('./db');
const Routes = require('./routes');
const Blipp = require('blipp');
const Inert = require('inert');
const UPLOAD_PATH = 'uploads';
const fs = require('fs');
const Seeder = require('./seed');
const SocketIO = require('socket.io');
const Evnts = require('./events');
const io = SocketIO.listen(Server.listener);

const startServer = async () => {
    try {
        await Database.init();
        await Server.register([Blipp, Inert]);
        await Routes.initRoutes(Server);
        await Server.start();

        let connectedUsers = [];
        io.sockets.on('connection', (socket) => {
            socket.on('register', (regPayload) => {
                if (regPayload && 'undefined' !== typeof regPayload.userId && regPayload.userId ) {
                    connectedUsers.push(regPayload.userId);
                }
            });
        });
        notifyOnApproval();
        notifyAdminOnStudentUpdate();
        notifyOnsuspension();
        notifyOnConfirm();
        notifyOnUnSuspend();
        Seeder.seedAdmin();
        log(`Server started at  port 5000 `);
    } catch (error) {
        logErr(error);
    }
}

if (!fs.existsSync(UPLOAD_PATH)) fs.mkdirSync(UPLOAD_PATH);
startServer();
function notifyOnApproval() {
    Evnts.registerEvent('application:approved', (data) => {
        io.sockets.emit(data.userId, {
            title: 'Scholarship Application',
            message: 'Your scholarship application is approved '
        })
    })
}

function notifyAdminOnStudentUpdate() {
     Evnts.registerEvent('scholar_profile:updated', (data) => {
        io.sockets.emit('student_profile_update', { scholarName: data.scholarName })
    })
}
function notifyOnsuspension() {
    Evnts.registerEvent('account:suspended', (data) => {
        io.sockets.emit(data.userId, {
            title: 'Account Suspended',
            message: 'Your account has been suspended please go to BCE office ASAP'
        })
    })
}

function notifyOnConfirm() {
    Evnts.registerEvent('account:confirmed', (data) => {
        io.sockets.emit(data.userId, {
            title: 'Account Confirmed',
            message: 'Your account has been confirmed you can now sumbit your application'
        })
    })
}


function notifyOnUnSuspend() {
    Evnts.registerEvent('account:unsuspend', (data) => {
        io.sockets.emit(data.userId, {
            title: 'Account Unsuspended',
            message: 'Your account has been successfully unsuspended !!'
        })
    })
}