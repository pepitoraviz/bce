var eventEmitter = new (require('events').EventEmitter)();

function emitEvent(str, data) {
    'use strict';
    eventEmitter.emit(str, data);
}

function registerEvent(str, callback) {
    'use strict';
    eventEmitter.on(str, callback);
}

function registerEventOnce(str, callback) {
    'use strict';
    eventEmitter.once(str, callback);
}

exports.emitEvent = emitEvent;
exports.registerEvent = registerEvent;
exports.registerEventOnce = registerEventOnce;