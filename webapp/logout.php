<?php
    session_start();
    unset($_SESSION['userId']);
    unset($_SESSION['role']);
    header("Location: login.php");
    die();