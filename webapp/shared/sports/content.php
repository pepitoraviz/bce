<section style="background: #009efd;padding:7em 0em;">
    <div class="container">
        <h2 class="text-white display-3">Sports Scholarship Application</h2>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-8 offset-2" style="padding:2em;">
                <div class="card">
                    <div class="card-body">
                        <form novalidate id="acadRequirements">
                            <div class="form-group">
                                <label>General Average</label>
                                <input type="number" name="genAve" id="genAve" class="form-control" min="0" max="100">
                            </div>
                            <div class="form-group">
                                <label>Birth Cert</label>
                                <input type="file" name="birthCert" id="birthCert" class="form-control" accept="application/pdf">
                            </div>
                            <div class="form-group">
                                <label>School Id</label>
                                <input type="file" name="schoolId" id="schoolId" class="form-control" accept="application/pdf">
                            </div>
                            <div class="form-group">
                                <label>Barangay Certificate</label>
                                <input type="file" name="barangayCertificate" id="barangayCertificate" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Certified true copy of grades or equivalent</label>
                                <input type="file" name="prevSemCard" id="prevSemCard" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Accomplishments</label>
                                <input type="file" name="accomplishments" id="accomplishments" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Sports Clinic Attendance</label>
                                <input type="file" name="attendanceSportsClinic" id="attendanceSportsClinic" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Coach Endorsment </label>
                                <input type="file" name="coachEndorsmentLetter" id="coachEndorsmentLetter" class="form-control">
                            </div>

                            <div class="form-group">
                                <label>Essay</label>
                                <input type="file" name="essay" id="essay" class="form-control">
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-warning btn-block">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="modal" tabindex="-1" role="dialog" id="applicationProgress">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="progress">
                    <div class="progress-bar progress-bar-striped" role="progressbar" style="width: 0%" aria-valuenow="10" aria-valuemin="0"
                        aria-valuemax="100" id="appLoader"></div>
                </div>
            </div>

        </div>
    </div>
</div>