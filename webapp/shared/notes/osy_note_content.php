<section style="background: #009efd;padding:7em 0em;">
    <div class="container">
        <h2 class="text-white display-1">Scholar Portal</h2>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-6 mx-auto " style="padding:2em;">
                <div class="jumbotron">
                    <h1 class="display-4">Reminders</h1>
                    <p class="lead">Please read the following instruction carefully. Not following instruction may result to unfavorable results or such</p>
                    <hr class="my-4">
                    <p>
                        Please only send single application per year (for elementary) or per semester (for tertiary). <span class="text-danger">SPAMMING</span> may result
                        to suspension of account and sunctions.
                    </p>
                    <p>
                        Please only upload a PDF format of the scanned documents, make sure that the document is readable.
                    </p>
                    <p>
                        Carefully choose applicable scholarship type because each type has different document requirement and it is needed to satisfy the appliation requirements
                    </p>
                    <p class="lead">
                        <a class="btn btn-primary btn-lg btn-block" href="osy.php" role="button">Proceed as elementary</a>
                    </p>

                     <p class="lead">
                        <a class="btn btn-primary btn-lg btn-block" href="secondary_osy.php" role="button">Proceed as secondary ( Highschool ) </a>
                    </p>

                     <p class="lead">
                        <a class="btn btn-primary btn-lg btn-block" href="osy_tertiary.php" role="button">Proceed as tertiary ( College or tech voc ) </a>
                    </p>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-6 mx-auto" style="padding:2em;">

            </div>

        </div>
    </div>
</section>