<div class="col-md-4">

  <!-- Search Widget -->
  <div class="card my-4">
    <h5 class="card-header">Search</h5>
    <div class="card-body">
      <form action="news.php" method="GET">
        <h4>Search title</h4>
        <div class="form-group">
          <input type="text" name="post_title" class="form-control" placeholder="Search for...">
        </div>

        <h4>Post date</h4>
        <div class="form-group">
          <input id="postDatePicker" name="post_date" class="form-control" />
        </div>
        <div class="form-group">
          <button class="btn btn-secondary btn-block">Search</button>
        </div>
      </form>
    </div>
  </div>

  <!-- Categories Widget -->


  <!-- Side Widget -->
  <div class="card my-4 archive-container">
    <h5 class="card-header">Archives</h5>
    <div class="card-body">
      <ul class="list-unstyled year-list" id="year-list">
        <!-- <li class="year-item">
                    <span class="year-label"><i class="year-icon fa fa-chevron-right"></i>2018</span>
                    <ul class="list-unstyled month-list d-none">
                      <li class="month-item">
                        <span class="month-label"> <i class="month-icon fa fa-chevron-right  "></i> May</span>
                        <ul class="list-unstyled article-list d-none">
                          <li class="article-item">Some articleasdasdasdasdsa</li>
                        </ul>
                      </li>
                      <li class="month-item">
                         <span class="month-label"> <i class="month-icon fa fa-chevron-right  "></i> April</span>
                      </li>
                      <li class="month-item">
                        <span class="month-label"> <i class="month-icon fa fa-chevron-right"></i> March</span>  
                      </li>
                    </ul>
                </li>
                <li class="year-item">
                    <span class="year-label"><i class="fa fa-chevron-right"></i>2017</span>
                </li> -->
      </ul>
    </div>
  </div>

</div>