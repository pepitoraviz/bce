    <footer class="py-5 bg-black">
      <div class="container">
        <p class="m-0 text-center text-white small">Copyright &copy; BCE Scholarship 2018</p>
      </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="/assets/vendors/jquery/jquery.min.js"></script>
    <script src="/assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/vendors/moment/moment.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.10/lodash.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>

    <script>
      var postTitle = '<?php echo isset($_GET["post_title"]) ? $_GET["post_title"] : '' ?>';
      var postDate = '<?php echo isset($_GET["post_date"]) ? $_GET["post_date"] : '' ?>';

    </script>
    <!-- Custom scripts for this template -->
    <script src="/assets/js/blog.js"></script>
    <script src="/assets/js/blog-list.js"></script>
    <script>
      var blogList = (function($){
          var $postContainer = $('#postContent');
          function init() {
            populateBlogList();
            createDatePicker();
          }
          function createPostListElem (postObj) {
            var el = '<div class="post-preview">'
            el += '<a href="/news_details.php?postId='+postObj._id+'">'
            el += '<h2 class="post-title">'
            el += postObj.title
            el += '</h2>'
            el += '</a>'
            el += '<p class="post-meta">'
            el += 'posted on '+ moment(new Date(postObj.createdAt)).format('ll')
            el += '</p>'
            el += '</div>'
            el += '<hr>'
            return el;
          }
          function populateBlogList(){
            if (!isBlank(postTitle) || !isBlank(postDate)) {
              populateBySearch();
            }else {
               populateDefault();
            }
            createArchives();
          }
          function populateDefault() {
            getAllPosts()
                .then(function(posts) {
                    if (Array.isArray(posts)){
                        posts.forEach(function(postObj) {
                          $postContainer.append(createPostListElem(postObj));
                        })
                    }

                })
          }
          function createArchives() {
              getAllPosts()
                .then(function(posts) {
                  var archives = chuckPost(posts);
                  renderArchive(archives);
                })
          }
          function populateBySearch() {
            var searhcBy = {};
            if (!isBlank(postTitle)) {
              searhcBy['postTitle'] = postTitle;
            }

            if (!isBlank(postDate)) {
              searhcBy['postDate'] = postDate;
            }
            var queryString = createQueryString(searhcBy);
            getByQuery(queryString)
              .then(function(posts){
                 if (Array.isArray(posts)){
                        posts.forEach((postObj) => {
                          $postContainer.append(createPostListElem(postObj));
                        })
                    }

              })
          }
          function getByQuery(query) {
            return $.ajax({
              type: 'GET',
              url: 'http://localhost:5000/api/posts/getByQuery'+query
            })
          }
          function createQueryString(queryObj) {
            return '?' +
            Object.keys(queryObj).map(function(key) {
                return encodeURIComponent(key) + '=' +
                    encodeURIComponent(queryObj[key]);
            }).join('&');
          }
          function isBlank(str) {
              return (!str || /^\s*$/.test(str));
          }
          function createDatePicker() {
              $('#postDatePicker').datepicker({
                  uiLibrary: 'bootstrap4'
              });
          }
          function chuckPost(postCollection){

                if(Array.isArray(postCollection)) {
                  let groupedPost = _.groupBy(postCollection, (post) => {
                      return moment(post.createdAt).startOf('year').format('YYYY')
                  })

                  let a =Object.keys(groupedPost).forEach((a) =>  {
                    let c = _.groupBy(groupedPost[a], (p) => {
                      return moment(p.createdAt).startOf('month').format('MMMM')
                    })
                    groupedPost[a] = c;
                  });

                  return groupedPost;
                }
                return false;
          }
          function renderArchive(archives){
             var $yearContainer = $('#year-list');
             if(archives) {
               Object.keys(archives).forEach((year) => {
                  var yearElm = '<li class="year-item">';
                  yearElm += '<span class="year-label"><i class="year-icon fa fa-chevron-right"></i>'+year+'</span>'
                  yearElm += '<ul class="list-unstyled month-list d-none" id="month-list">'
                  Object.keys(archives[year]).forEach((month) => {
                       yearElm += '<li class="month-item">'
                       yearElm += ' <span class="month-label"> <i class="month-icon fa fa-chevron-right  "></i>'+month+'</span>'
                       yearElm += renderMonths(month, year, archives);
                       yearElm += '</li>'
                  })
                  yearElm+= '</ul>'
                  yearElm += '<li>'
                 $yearContainer.append(yearElm)

               })
             }
          }
          function renderMonths(month, yearObj, archives) {
            let archiveItems = archives[yearObj][month];
            let archiveElm = '<ul class="list-unstyled article-list d-none">'
              archiveItems.forEach((item) => {
                archiveElm += '<li class="article-item">'
                archiveElm += '<a href="/news_details.php?postId='+item._id+'">'+item.title+'</a>'
                archiveElm += '</li>'
              })
            archiveElm += '</ul>'
            return archiveElm;
          }
          function getAllPosts(){
            return $.ajax({
              method: 'GET',
              url:'http://localhost:5000/api/posts'
            })
          }
          return {
            init: init
          }
      })(jQuery)

      $(document).ready(() => {
        blogList.init()
      })

    </script>
  </body>

</html>
