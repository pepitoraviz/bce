    <header class="masthead text-center text-white">
      <div class="masthead-content">
        <div class="container">
                      <img src="assets/img/logo.png" alt="" style="display:inline-block;margin-right:20px;width:250px;">
          <h1 class="masthead-heading mb-0">BCE Scholarship Progam</h1>
          <h2 class="masthead-subheading mb-0">Batang commited sa edukasyon</h2>
          <a href="be_scholar.php" class="btn btn-primary btn-xl rounded-pill mt-5">Be a scholar</a>
        </div>
      </div>
      <div class="bg-circle-1 bg-circle"></div>
      <div class="bg-circle-2 bg-circle"></div>
      <div class="bg-circle-3 bg-circle"></div>
      <div class="bg-circle-4 bg-circle"></div>
    </header>