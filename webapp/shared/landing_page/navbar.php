<?php
session_start();
function isActive($page)
{
    $fileName = basename($_SERVER['PHP_SELF']);
    return $page == $fileName ? 'active' : 'wew';
}
?>
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand" href="index.php">BCE Scholarship</a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fa fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item <?php echo isActive('index.php'); ?>">
                        <a class="nav-link" href="index.php">Home</a>
                    </li>
                    <li class="nav-item <?php echo isActive('news.php'); ?>">
                        <a class="nav-link" href="news.php">News</a>
                    </li>
                    <?php if (!isset($_SESSION['userId'])) { ?>
                    <li class="nav-item <?php echo isActive('be_scholar.php'); ?>">
                        <a class="nav-link" href="be_scholar.php">Be a scholar</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="login.php">Login</a>
                    </li>
                    <?php 
                } ?>
                    <?php if ('SCHOLAR' === $_SESSION['role']) { ?>
                        <li class="nav-item <?php echo isActive('application.php'); ?>">
                            <a class="nav-link" href="application.php">My Application</a>
                        </li>
                         <li class="nav-item <?php echo isActive('me.php'); ?> <?php echo isActive('my_notifs.php'); ?>">
                            <a class="nav-link" href="me.php">My Account</a>
                        </li>

                    <?php 
                } ?>

                    <?php if ('ADMIN' === $_SESSION['role']) { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/admin/posts.php">Admin Panel</a>
                        </li>
                    <?php 
                } ?>

                    <?php if (isset($_SESSION['userId'])) { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="logout.php">Logout</a>
                        </li>
                    <?php 
                } ?>


                </ul>
            </div>
        </div>
    </nav>