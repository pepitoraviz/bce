<script src="assets/vendors/jquery/jquery.min.js"></script>
<script src="assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/blog.js"></script>
<script src="/assets/js/validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.dev.js"></script>
<script>
      var userId = "<?php echo $_SESSION['userId']?>";
</script>
<script src="/assets/js/student_notif.js"></script>
<script>
  $(document).ready(function () {
    meApp.init();
    if (notifApp) {
      notifApp.init();
    }
  })
  var meApp = (function () {
      var socket, notif, notifLength = 0;
      var $myNotif = $('#myNotif');
      var $email = $('#email'),
      $password = $('#password'),
      $newPass = $('#newPass'),

      $meFirstName = $('#meFirstName'),
      $meMiddleName = $('#meMiddleName'),
      $meLastName = $('#meLastName'),
      $meSuffix = $('#meSuffix'),
      $meBarangay = $('#meBarangay'),
      $contactNumber = $('#contactNumber'),
      $meAddress = $('#meAddress'),
      $meSchoolName = $('#meSchoolName'),
      $meSchoolAddress = $('#meSchoolAddress')
      $moFirstName = $('#moFirstName'),
      $moMiddleName = $('#moMiddleName'),
      $moLastName = $('#moLastName'),
      $moAge = $('#moAge'),
      $moContact = $('#moContact'),
      $moOccupation = $('#moOccupation')
      $moBarangay = $('#moBarangay'),
      $moAddress = $('#moAddress')
      $faFirstName = $('#faFirstName'),
      $faMiddleName = $('#faMiddleName'),
      $faLastName = $('#faLastName'),
      $faSuffix = $('#faSuffix'),
      $fAge = $('#fAge'),
      $faContact = $('#faContact')
      $faOccupation = $('#faOccupation'),
      $faBarangay = $('#faBarangay'),
      $faAddress = $('#faAddress'),
      $personalInfoForm = $('#personalInfoForm'),
      $parentForm = $('#parentForm');

    function init() {
      scrollToSection();
      getProfile();
      setupValidation();
      createBarangayOpts();
      onSaveProfile();
      onSaveParent();
    }
  
    function scrollToSection() {
      $(document).on('click', '.info-nav-item span', function (e) {
        e.preventDefault();
        var target = $(this).data('targ');
        $('html, body').animate({
          scrollTop: $(`#${target}`).offset().top + "px"
        });
      })
    }

    function getProfile() {
      if (userId) {
        fetchProfile(userId)
          .then((resp) => {
            setConfirm(resp.isConfirmed)
            setPersonalInfo(resp);
            setAccountInfo(resp);
            setMotherInfo(resp);
            setFatherInfo(resp);
            removeNotifCount();
          })
      }
    }

    function setupValidation() {
      $personalInfoForm.validate({
        rules: {
          meFirstName: 'required',
          contactNumber: 'required',
          meLastName: 'required',
          meBarangay: 'required',
          meAddress: 'required',
          meSchoolName: 'required',
          meSchoolAddress: 'required',
        }
      });
      $parentForm.validate({
         rules:{
            moFirstName: 'required',
          moMiddleName: 'required',
          moLastName: 'required',
          faFirstName: 'required',
          moAge: 'required',
          moContact: 'required',
          moOccupation: 'required',
          moBarangay: 'required',
          moAddress: 'required',
          faFirstName: 'required',
          faMiddleName: 'required',
          faLastName: 'required',
          fAge: 'required',
          faContact: 'required',
          faBarangay: 'required',
          faAddress: 'required'
         }
      });
    }

    function onSaveProfile() {
      $personalInfoForm.on('submit', function (e) {
        e.preventDefault();
        var meObj = {
          firstName: $meFirstName.val(),
          contactNumber: $contactNumber.val(),
          middleName: $meMiddleName.val(),
          lastName: $meLastName.val(),
          suffix: $meSuffix.val(),
          barangay: $meBarangay.val(),
          address: $meAddress.val(),
          schoolName: $meSchoolName.val(),
          schoolAddress: $meSchoolAddress.val()
        }

        meObj.role = "SCHOLAR";
        if ($personalInfoForm.valid()){
            updateUser(meObj, userId)
              .then(() => window.location.href = 'me.php');
        }
      })
    }

    function onSaveParent() {
      $parentForm.on('submit', function(e){
        e.preventDefault();
        var parentObj = {
            faAge: $fAge.val(),
            moFirstName: $moFirstName.val(),
            moMiddleName: $moMiddleName.val(),
            moLastName: $moLastName.val(),
            moAge: $moAge.val(),
            moContact: $moContact.val(),
            moOccupation: $moOccupation.val(),
            moBarangay: $moBarangay.val(),
            moAddress: $moAddress.val(),
            faFirstName: $faFirstName.val(),
            faMiddleName: $faMiddleName.val(),
            faLastName: $faLastName.val(),
            faSuffix: $faSuffix.val(),
            faContact: $faContact.val(),
            faOccupation: $faOccupation.val(),
            faBarangay: $faBarangay.val(),
            faAddress: $faAddress.val(),
        }

        if ($parentForm.valid()){
           updateUser(parentObj, userId)
              .then(() => window.location.href = 'me.php');
        }
      })
    }
  

  
    function removeNotifCount(){
      $myNotif.on('click', function(e) {
        e.preventDefault();
        var href = $(this).attr('href');
        $('#notifCount').remove();
        window.location.href = href;
      })
    }
    function setConfirm(isConfirmed) {
      if (isConfirmed) {
        $('#accStat').html('Confirmed').addClass('text-success');
      } else {
        $('#accStat').html('Unconfirmed').addClass('text-danger');
      }
    }

    function setAccountInfo(resp) {
      $email.val(resp.email);
      $password.on('focus', function () {
        $(this).val(null)
      })
    }

    function setPersonalInfo(resp) {
      $meFirstName.val(resp.firstName);
      $contactNumber.val(resp.contactNumber);
      $meMiddleName.val(resp.middleName || null);
      $meLastName.val(resp.lastName || null);
      $meSuffix.val(resp.suffix || null);
      $meAddress.val(resp.address || null);
      $meBarangay.val(resp.barangay || null);
      $meSchoolAddress.val(resp.schoolAddress || null);
      $meSchoolName.val(resp.schoolName || null);
    }

    function setMotherInfo(resp) {
      $moFirstName.val(resp.moFirstName || null)
      $moMiddleName.val(resp.moMiddleName || null)
      $moLastName.val(resp.moLastName || null)
      $moAge.val(resp.moAge || null)
      $moContact.val(resp.moContact || null)
      $moOccupation.val(resp.moOccupation || null)
      $moBarangay.val(resp.moBarangay || null)
      $moAddress.val(resp.moAddress || null)
    }


    function setFatherInfo(resp) {
      $faFirstName.val(resp.faFirstName || null)
      $faMiddleName.val(resp.faMiddleName || null)
      $faLastName.val(resp.faLastName || null)
      $faSuffix.val(resp.faSuffix || null)
      $fAge.val(resp.faAge || null)
      $faContact.val(resp.faContact || null)
      $faOccupation.val(resp.faOccupation || null)
      $faBarangay.val(resp.faBarangay || null)
      $faAddress.val(resp.faAddress || null)
    }

    function fetchProfile(userId) {
      return $.ajax({
        method: 'GET',
        url: 'http://localhost:5000/api/users/' + userId,
      })
    }

    function createBarangayOpts () {
          var barangays = [{
                           text: 'Bagong Ilog',
                val: 'bagong_ilog'
            },
            {
            text: 'Pinag Buhatan',
                val: 'pinag_buhatan'
            },
            {
                text: 'Bagong Katipunan',
                val: 'bagong_katipunan'
            },
            {
                text: 'Bambang',
                val: 'bambang'
            },
            {
                text: 'Buting',
                val: 'buting'
            },
            {
                text: 'Caniogan',
                val: 'caniogan'
            },
            {
                text: 'Kapitolyo',
                val: 'kapitolyo'
            },
            {
                text: 'Kapasigan',
                val: 'kapasigan'
            },
            {
                text: 'Malinao',
                val: 'malinao'
            },
            {
                text: 'Oranbo',
                val: 'oranbo'
            },
            {
                text: 'Palatiw',
                val: 'palatiw'
            },
            {
                text: 'Pineda',
                val: 'pineda'
            },
            {
                text: 'Sagad',
                val: 'sagad'
            },
            {
                text: 'San Antonio',
                val: 'san_antonio'
            },
            {
                text: 'San Joaquin',
                val: 'san_joaquin'
            },
            {
                text: 'San Jose',
                val: 'san_jose'
            },
            {
                text: 'San Nicolas',
                val: 'san_nicolas'
            },
            {
                text: 'Sta. Cruz',
                val: 'sta_cruz'
            },
            {
                text: 'Sta. Rosa',
                val: 'sta_rosa'
            },
            {
                text: 'Sto. Tomas',
                val: 'sto_tomas'
            },
            {
                text: 'Sumilang',
                val: 'sumilang'
            },
            {
                text: 'Ugong',
                val: 'ugong'
            },
          ]
          var $barangayInput = $('#barangay');
          barangays.forEach((b) => {
            $moBarangay.append('<option value="'+b.val+'">'+b.text+'</option>');
            $faBarangay.append('<option value="'+b.val+'">'+b.text+'</option>');
            $meBarangay.append('<option value="'+b.val+'">'+b.text+'</option>');
          })
        }
    function updateUser(data, userId) {
      return $.ajax({
        method: 'PUT',
        url: 'http://localhost:5000/api/users/' + userId,
        data: data
      })
    }
    return {
      init: init
    }
  })(jQuery);
</script>
</body>

</html>