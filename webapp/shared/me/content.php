<section style="background: #009efd;padding:7em 0em;">
    <div class="container">
             <div class="header-container" style="display:flex;align-items:center;align-content:center;">
            <img src="assets/img/logo.png" alt="" style="display:inline-block;margin-right:20px;width:100px;">
        <h2 class="text-white display-1">Scholar Portal</h2>
       </div>
    </div>
</section>

<section>


    <div style=" padding:2em 3em;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8 mx-auto">
                     <ul class="list-unstyled">
                            <li style="padding:1em;" class="info-nav-item">
                                <span style="cursor:pointer;" data-targ="acc-info">Account Information</span>
                            </li>
                            <li style="padding:1em;" class="info-nav-item">
                                <span style="cursor:pointer;" data-targ="personal-info">Personal Information</span>
                            </li>
                            <li style="padding:1em;" class="info-nav-item">
                                <span style="cursor:pointer;" data-targ="parent-info">Parent  Information</span>
                            </li>
                            <li style="padding:1em;" class="info-nav-item">
                                <a href="/my_notifs.php" style="text-decoration:none;color:#000;" id="myNotif">My notifications</a>
                            </li>
                        </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 mx-auto" id="acc-info">
                    <div class="card">
                        <div class="card-body p-5">
                            <div style="display:flex;align-items:center;justify-content:space-between;">
                                <!-- <h2 style="margin:0;">Account Status :</h2> -->
                                <!-- <span style="font-size:2em;" id="accStat"></span> -->
                            </div>
                            <h2 class="text-warning">Account Information</h2>
                            <form novalidate id="accForm">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" id="email" name="email" disabled>
                                </div>


                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-12 col-sm-12">
                                            <label>Current Password</label>
                                            <input type="password" class="form-control" name="password" id="password" value="password">
                                        </div>
                                        <div class="col-lg-6 col-md-12 col-sm-12">
                                            <label>New Password</label>
                                            <input type="password" class="form-control" name="newPass" id="newPass">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-warning btn-lg">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row" style="margin-top:1.5em;">
                <div class="col-md-8 mx-auto">
                    <div class="card">
                        <div class="card-body p-5" id="personal-info">
                            <form id="personalInfoForm">
                                <h2 class="text-warning">Personal Information</h2>
                                <div class="form-group">
                                    <label>Name</label>
                                    <div class="row">
                                        <div class="col-lg-3 col-md-12 col-sm-12">
                                            <input type="text" class="form-control" placeholder="First name" name="meFirstName" id="meFirstName">
                                        </div>

                                        <div class="col-lg-3 col-md-12 col-sm-12">
                                            <input type="text" class="form-control" placeholder="Middle name" name="meMiddleName" id="meMiddleName">
                                        </div>

                                        <div class="col-lg-3 col-md-12 col-sm-12">
                                            <input type="text" class="form-control" placeholder="Last name" name="meLastName" id="meLastName">
                                        </div>
                                        <div class="col-lg-3 col-md-12 col-sm-12">
                                            <input type="text" class="form-control" placeholder="Suffix" name="meSuffix" id="meSuffix">
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label>Barangay</label>
                                    <select name="meBarangay" id="meBarangay" class="form-control">
                                        <option value="null" disabled selected>Select Barangay</option>
                                    </select>
                                </div>

                                 <div class="form-group">
                                    <label>Cellphone Number</label>
                                    <input type="text" class="form-control" name="contactNumber" id="contactNumber">
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea rows="5" class="form-control" name="meAddress" id="meAddress"></textarea>
                                </div>


                                <div class="form-group">
                                    <label>School Name</label>
                                    <input type="text" class="form-control" name="meSchoolName" id="meSchoolName">
                                </div>
                                <div class="form-group">
                                    <label>School Address</label>
                                    <textarea rows="5" class="form-control" name="meSchoolAddress" id="meSchoolAddress"></textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-warning btn-lg">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top:1.5em;">
                <div class="col-md-8 mx-auto">
                    <div class="card">
                        <div class="card-body p-5" id="parent-info">
                            <form id="parentForm" novalidate>
                                <h2 class="text-warning">Parent Information</h2>
                                <h4>Mother Information</h4>
                                <div class="form-group">
                                    <label>Name</label>
                                    <div class="row">
                                        <div class="col-lg-5 col-md-12 col-sm-12">
                                            <input type="text" class="form-control" placeholder="First name" name="moFirstName" id="moFirstName">
                                        </div>

                                        <div class="col-lg-3 col-md-12 col-sm-12">
                                            <input type="text" class="form-control" placeholder="Middle name" name="moMiddleName" id="moMiddleName">
                                        </div>

                                        <div class="col-lg-4 col-md-12 col-sm-12">
                                            <input type="text" class="form-control" placeholder="Last name" name="moLastName" id="moLastName">
                                        </div>

                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-12 col-sm-12">
                                            <label>Age</label>
                                            <input type="text" class="form-control" placeholder="Age" name="moAge" id="moAge">
                                        </div>

                                        <div class="col-lg-3 col-md-12 col-sm-12">
                                            <label>Contact Number</label>
                                            <input type="text" class="form-control" placeholder="Contact Number" name="moContact" id="moContact">
                                        </div>

                                        <div class="col-lg-7 col-md-12 col-sm-12">
                                            <label>Occupation</label>
                                            <input type="text" class="form-control" placeholder="Occupation" name="moOccupation" id="moOccupation">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Barangay</label>
                                    <select name="moBarangay" id="moBarangay" class="form-control">
                                        <option value="null" disabled selected>Select Barangay</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea rows="5" class="form-control" name="moAddress" id="moAddress"></textarea>
                                </div>

                                <h4>Father Information</h4>

                                <div class="form-group">
                                    <label>Name</label>
                                    <div class="row">
                                        <div class="col-lg-4 col-md-12 col-sm-12">
                                            <input type="text" class="form-control" placeholder="First name" name="faFirstName" id="faFirstName">
                                        </div>

                                        <div class="col-lg-2 col-md-12 col-sm-12">
                                            <input type="text" class="form-control" placeholder="Middle name" name="faMiddleName" id="faMiddleName">
                                        </div>

                                        <div class="col-lg-4 col-md-12 col-sm-12">
                                            <input type="text" class="form-control" placeholder="Last name" name="faLastName" id="faLastName">
                                        </div>
                                        <div class="col-lg-2 col-md-12 col-sm-12">
                                            <input type="text" class="form-control" placeholder="Suffix" name="faSuffix" id="faSuffix">
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-2 col-md-12 col-sm-12">
                                            <label>Age</label>
                                            <input type="text" class="form-control" placeholder="Age" name="fAge" id="fAge">
                                        </div>

                                        <div class="col-lg-3 col-md-12 col-sm-12">
                                            <label>Contact Number</label>
                                            <input type="text" class="form-control" placeholder="Contact Number" name="faContact" id="faContact">
                                        </div>

                                        <div class="col-lg-7 col-md-12 col-sm-12">
                                            <label>Occupation</label>
                                            <input type="text" class="form-control" placeholder="Occupation" name="faOccupation" id="faOccupation">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Barangay</label>
                                     <select name="faBarangay" id="faBarangay" class="form-control">
                                        <option value="null" disabled selected>Select Barangay</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea rows="5" class="form-control" name="faAddress" id="faAddress"></textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-warning btn-lg">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>