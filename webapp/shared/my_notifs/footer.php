<script src="assets/vendors/jquery/jquery.min.js"></script>
<script src="assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/blog.js"></script>
<script src="/assets/vendors/moment/moment.js"></script>
<script src="/assets/vendors/datatables/jquery.dataTables.js"></script>
<script src="/assets/vendors/datatables/dataTables.bootstrap4.js"></script>


<script>
    $(document).ready(function() {
        myNotifs.init();
    })
</script>
<script>
    var myNotifs = (function($) {
        var userId = "<?php echo $_SESSION['userId']?>";
        $notifGroup = $('#notifGroup');
        function init() {
            listNotifs();
            readAllNotif();
        }
        function listNotifs() {
            getAllNotifs()
                .then((resp) => {
                    if (Array.isArray(resp)) {
                        resp.forEach((notif) => {
                            var notifEl = generateEl(notif);
                            $notifGroup.append(notifEl);
                        })
                    }
                })
        }
        function generateEl(notif) {
            var el = "";
            el += '<li class="list-group-item list-group-item-action flex-column align-items-start ">'
            el += '<div class="d-flex w-100 justify-content-between">'
            el += '<h5 class="mb-1">'+notif.title+'</h5>'
            el += '<small>'+moment(notif.createdAt).fromNow();+'</small>'
            el += '</div>'
            el += '<p class="mb-1">'+notif.content+'</p>'
            el += '</li>'
            return el;
        }
        function getAllNotifs() {
            return $.ajax({
                type: 'GET',
                url: 'http://localhost:5000/api/student_notifs/'+userId
            })
        }
        function readAllNotif(){
            return $.ajax({
                        type: 'GET',
                        url: 'http://localhost:5000/api/student_notifs/'+userId+'/readall'
            })
        }
        return {
            init: init
        }
    })(jQuery)
</script>
</body>

</html>