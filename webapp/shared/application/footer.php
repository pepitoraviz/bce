<script src="assets/vendors/jquery/jquery.min.js"></script>
<script src="assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/blog.js"></script>
<script src="/assets/vendors/moment/moment.js"></script>
<script src="/assets/vendors/datatables/jquery.dataTables.js"></script>
<script src="/assets/vendors/datatables/dataTables.bootstrap4.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.dev.js"></script>

<script>
      var userId = "<?php echo $_SESSION['userId']?>";
</script>
<script src="/assets/js/student_notif.js"></script>
<script>
        $(document)
            .ready(() => {
                app.init()
                if (notifApp) {
                    notifApp.init();
                }
            })
            var app = (function($){
            var $appTbl = $('#appTbl'),
            dtTbl, $accountStat= $('#accountStat'),
            $accountNote = $('#accountNote'),
            currentScholar ;
            function init() {
             createDt();
              populateTbl();
              fetchProfile()
                .then(function(resp) {
                    currentScholar = resp;
                    showSendApp();
                    var statEl = getStatus(resp);
                    createNote(resp);
                    assesAccount(resp);
                    $accountStat.append(statEl);
                })
            }

            function getStatus(scholar) {
                    var stat = '';
                    if (scholar.isConfirmed && !scholar.isSuspended) {
                        stat = '    <span class="text-success">Confirmed</span>'
                    }

                    if (!scholar.isConfirmed) {
                        stat = ' <span class="text-warning">Unconfirmed</span>'
                    }

                    if(scholar.isSuspended) {
                    stat = ' <span class="text-danger">Suspended</span>'
                    }
                    return stat;
            }
            function createNote(scholar) {
                    $accountNote.html('');
                    var noteEl = '';
                    if (scholar.isConfirmed && !scholar.isSuspended) {
                        noteEl = '<span class="text-dark">Your account is confirmed, you can now send your scholarship application</span>'
                    }

                    if (!scholar.isConfirmed) {
                        noteEl = '<span class="text-dark">Your account is not yet confirmed you may not pass your appplication until it is confirmed by the BCE Admins</span>'
                    }

                    if(scholar.isSuspended) {
                        noteEl = '<span class="text-dark">Your account is currently suspended , we advice that you must visit BCE office immediately to resolve the suspension issue</span>'
                    }
                    $accountNote.append(noteEl);
            }
            function assesAccount(profile) {
                var $sendApplication = $('#sendApplication');

                if(profile && $sendApplication.length > 0) {
                    if(profile.isConfirmed && !profile.isSuspended) {
                        $sendApplication.attr('disabled', false);
                    }

                    if(!profile.isConfirmed) {
                        $sendApplication.attr('disabled', true);
                    }


                    if(profile.isSuspended) {
                        $sendApplication.attr('disabled', true);
                    }
                }
            }
            function showSendApp () {
                $(document).on('click', '#sendApplication', function(e) {
                    if(false === currentScholar.isSuspended && currentScholar.isConfirmed) {
                        $('#applicationType').modal('show');

                    }
                })
            }
            function createDt(){
                    dtTbl = $appTbl.DataTable();
            }
            function fetchProfile() {
                return $.ajax({
                    method: 'GET',
                    url: 'http://localhost:5000/api/users/' + userId,
                })
            }
            function populateTbl() {
                fetchMyApps()
                    .then((resp) => {
                        if (Array.isArray(resp)) {
                            resp.forEach((app) => {
                                dtTbl.row.add([
                                    moment(app.createdAt).format('ll'),
                                    app.isApproved ? '<span class="text-success">Approved</span>' : '<span class="text-warning">Pending</span>',
                                    app.genAve,
                                    app.schoolYear
                                ])
                                .draw();
                            })
                        }
                    })
            }
            function fetchMyApps() {
                return $.ajax({
                    type: 'GET',
                    url:'http://localhost:5000/api/application/'+userId+'/getAll'
                })
            }
            return {
                init: init
            }
        })(jQuery)
</script>
</body>
</html>