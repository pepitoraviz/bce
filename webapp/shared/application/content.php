<section style="background: #009efd;padding:7em 0em;">
    <div class="container">
       <div class="header-container" style="display:flex;align-items:center;align-content:center;">
            <img src="assets/img/logo.png" alt="" style="display:inline-block;margin-right:20px;width:100px;">
        <h2 class="text-white display-1">Scholar Portal</h2>
       </div>
    </div>
</section>

<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-10  mx-auto " style="padding:2em;">
                <h2>My Applications</h2>
                <div class="card card-block bg-light text-dark">
                    <div class="card-body">
                        <h4 id="accountStat">Account Status :</h4>
                        <p id="accountNote"></p>
                        <p id="statusChangeNote"></p>
                    </div>
                </div>
            </div>

        </div>
            <div class="row">
            <div class="col-10 text-right mx-auto " style="padding:2em;">
                <button class="btn btn-warning btn-lg"  id="sendApplication">Send Application</button>
            </div>

        </div>

        <div class="row">
            <div class="col-10 offset-1">
                <table class="table" id="appTbl">
                    <thead>
                        <tr>
                            <th>Date Submited</th>
                            <th>Status</th>
                            <th>Gen Ave</th>
                            <th>School Year</th>
                            <!-- <th>Action</th> -->
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
</section>


<div class="modal" tabindex="-1" role="dialog" id="applicationType">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Application Type </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <a href="academic_note.php" class="btn btn-block btn-warning">Academinc</a>
        <a  href="sports_note.php" class="btn btn-block btn-warning">Sports</a>
        <a  href="osy_note.php" class="btn btn-block btn-warning">Out of school youth</a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>