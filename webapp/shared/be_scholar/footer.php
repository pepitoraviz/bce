      <footer class="py-5 bg-black">
        <div class="container">
          <p class="m-0 text-center text-white small">Copyright &copy; BCE Scholarship 2018</p>
        </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="/assets/vendors/jquery/jquery.min.js"></script>
    <script src="/assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/vendors/moment/moment.js"></script>

    <!-- Custom scripts for this template -->
    <script src="/assets/js/blog.js"></script>

  </body>

</html>
