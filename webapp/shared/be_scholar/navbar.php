<?php
    function isActive($page){
        $fileName = basename($_SERVER['PHP_SELF']);
        return $page == $fileName ? 'active' : 'wew';
    }
?>
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
    <a class="navbar-brand" href="index.php">BCE Scholarship</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fa fa-bars"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
        <li class="nav-item <?php echo isActive('index.php');?>">
            <a class="nav-link" href="index.php">Home</a>
        </li>
        <li class="nav-item <?php echo isActive('news_details.php');?>">
            <a class="nav-link" href="news.php">News</a>
        </li>
        <li class="nav-item <?php echo isActive('be_scholar.php');?>">
            <a class="nav-link" href="be_scholar.php">Be a scholar</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Login</a>
        </li>
        </ul>
    </div>
    </div>
</nav>