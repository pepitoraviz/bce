<article>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto p-5">

        <h2> BASIC QUALIFICATIONS OF BCE SCHOLARS </h2>

        <div>
          <div>The basic qualification criteria for the scholarship program are:</div>

          <ol>
            <li>Must be children of bona fide residents of Pasig City for at least 5 years preceding the application;</li>
            <li>Must be children of retered Pasig City voters. However, once the applicant / scholar reaches 18 years old, he/
              she must also be a registered voter of Pasig.</li>
            <li>With a family income of up to P 10,000.00 a month;</li>
            <li>Possess good moral character;</li>
            <li>Academic scholars must have a weighted general average of 2.50 or 82 in the immediately preceding semester/ school
              yea, r. This minimum grade requirement, however does not apply to the sports and former OSY scholars but must
              not have a failing grade in any subject; and</li>
            <li>Must pass the interview (New applicants only).</li>
          </ol>
          <div>
            <span style="font-size: 10pt;">In addition, tertiary level applicants must have a minimum regular load of 16 units , except if graduating, and
              must not have &ldquo;dropped&rdquo; any subject or incurred a grade of &ldquo;Incomplete&rdquo; in the immediately
              preceding semester.</span>
          </div>
          <div>
            <p>
              <span style="font-size: 10pt;">Likewise it must be made clear that the average grade of 82 or 2.50 is only the minimum requirement for applicants.
                Actually this grade can go up higher depending upon the grades of all the other applicants because of the
                ranking system that is practiced. The higher the applicant&rsquo;s general average, the higher are his chances
                to be chosen as a BCE scholar.</span>
            </p>
          </div>
          <h3>DOCUMENTARY REQUIREMEMENTS</h3>
          <h4>New Applicants</h4>
          <div>1. Duly accomplished BCE application form with 1&rdquo;x 1&rdquo; recent picture of applicant</div>
          <div>The application form may be</div>
          <div>2. Proof of Identity</div>
          <br />
          <ol>
            <li>1) Birth Certificate</li>
            <li>2) School ID</li>
          </ol>
          <div>3. Proof of Residence</div>
          <ol>
            <li>1) Barangay Certificate</li>
            <li>2) Voters ID or VRR of parents / applicant&rsquo;s own Voters ID if 18 years old</li>
            <li>3) Parent&rsquo;s current year cedula</li>
          </ol>
          <div>4. Proof Academic Standing</div>
          <ol>
            <li>1) Certified True Copy of School Report Card (F 138) or Student&rsquo;s Permanent Record (F 137) in the absence
              of the original document</li>
            <li>2) School Enrolment Slip or Class Assignment List for elementary</li>
          </ol>
          <div>5. Additional for Tertiary Level only</div>
          <ol>
            <li>Current semester registration form</li>
            <li>Previous semester registration form</li>
            <li>Official Study Load for PLP students only</li>
            <li>Official Receipt (OR) of payment of tuition</li>
            <li>Previous semester class cards or original copy of Official Grade Report or computer printout of grades</li>
          </ol>
          <div>6. Others</div>
          <ol>
            <li>Self- addressed envelop with postage stamp</li>
            <li>One (1 ) long brown envelop to hold the application form and documents</li>
          </ol>
          <h4>Renewal and Returning Scholars</h4>
          <div>Duly accomplished BCE Scholarship Application Form with 1&rdquo;X1&rdquo; recent ID picture</div>
          <ol>
            <li>BCE Scholar&rsquo;s or School ID</li>
            <li>Voter&rsquo;s ID of one parent and of the applicant if 18 yrs old and above</li>
            <li>Parent&rsquo;s current year Cedula</li>
            <li>Original or certified true copy of School Report Card (F 138) or Student&rsquo;s Permanent Record (F 137) during
              the immediately preceding semester / school year</li>
            <li>School Enrolment Slip or Class Assignment List for Elementary only</li>
            <li>School Registration Forms - current &amp; previous semesters</li>
            <li>Official Study Load ( OSL ) for PLP students only</li>
            <li>Official Receipt of Payment ( OR )</li>
            <li>Original copy of class cards or Official Grade Report or Computer printout of grades</li>
            <li>Self-addressed envelope with postage stamp</li>
            <li>One (1) long brown envelop</li>
          </ol>
          <h4>Application Instruction</h4>
          <ol>
            <li>Print / write clearly all information required in the application form.</li>
            <li>Do not leave any information unanswered. Incomplete application, either incomplete entries or attachments, shall
              not be accepted or if inadvertently accepted, will not be processed.</li>
            <li>Put the application form, essay and all documents inside one long brown envelop. Print the following information
              on the face of the envelop in the following order:</li>
          </ol>
          <ol style="list-style-type: lower-alpha;">
            <li>Applicant&rsquo;s Last, Middle and First Names</li>
            <li>Age</li>
            <li>Academic Level</li>
            <li>Complete Home Address and Barangay</li>
            <li>Birthdate</li>
            <li>Birthplace</li>
            <li>School</li>
            <li>Course</li>
            <li>Present Year or Grade level</li>
            <li>Email Address</li>
            <li>Contact Nos.</li>
            <li>Father&rsquo;s Name</li>
            <li>Occupation</li>
            <li>Salary/ Income</li>
            <li>Mother&rsquo;s Name</li>
            <li>Opccupation</li>
            <li>Salary / Income</li>
          </ol>
          <h4>Sports Scholars</h4>
          <ol>
            <li>Duly accomplished application form with 1&rdquo;x 1&rdquo; ID picture</li>
            <li>Original or certified true copy of grades in the previous school year/ semester</li>
            <li>Report of accomplishments in the previous school year / semester</li>
            <li>Attendance in the Summer Sports Clinic conducted by the LGU</li>
            <li>Endorsement by the coach concerned</li>
          </ol>
          <h3>MAINTAINING THE SCHOLASHIP</h3>
          <p>To maintain the scholarship, scholars must submit the application form with the required documents every semester
            / school year.</p>
          <p>SUBMISSION OF APPLICATION FORM</p>
          <p>Submit the application form with the essay and all the required supporting documents at the BCE Scholars Office
            is located at the 2nd floor, Pasig Business Center Bldg. , Pasig City Hall Complex, Pasig City with telephone
            number 628 3478. INCOMPLETE APPICATION FORM ( with either incomplete entries or attachments ) shall not be accepted
            or if inadvertently accepted will not be processed.</p>
        </div>
        <div class="col-md-12">
          <a class="btn btn-warning btn-xl btn-block" style="border-radius:5rem;" href="register.php">Register Now</a>
        </div>
      </div>
    </div>
  </div>
</article>

<hr>