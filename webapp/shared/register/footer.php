<script src="assets/vendors/jquery/jquery.min.js">
</script>
<script src="assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/validate.js"></script>

<script>
  $(document).ready(function () {
    reg.init();
  })
  var reg = (function ($) {
    var $regForm = $('#regForm');

    function init() {
      setupValidation();
      onRegister();
      createBarangayOpts();
    }

    function setupValidation() {
      $regForm.validate({
        rules: {
          email: {
            required: true,
            email: true
          },
          contactNumber: "required",
          password: "required",
          confirm: {
            required: true,
            equalTo: "#password"
          },
          firstName: "required",
          lastName: "required",
          barangay: "required",
          address: "required",
          schoolName: "required",
          schoolAddress: "required"
        }
      })
    }

    function onRegister() {
      $('body').on('submit', '#regForm', (e) => {
        e.preventDefault();
        var data = $regForm.serializeArray().reduce(function (obj, item) {
          obj[item.name] = item.value;
          return obj;
        }, {});
        data.role = "SCHOLAR"
        register(data)
          .then((registerdData) => {
            $regForm[0].reset();
            setSesh(registerdData)
              .then((resp) => {
                window.location.href = '/me.php';
              })
          })
      })
    }

    function register(data) {
      return $.ajax({
        method: 'POST',
        url: 'http://localhost:5000/api/users/register',
        data: data
      })
    }

    function setSesh(userObj) {
      return $.ajax({
        method: 'POST',
        url: '/functions/set_session.php',
        data: userObj
      });
    }

    function createBarangayOpts() {
      var barangays = [{
          text: 'Bagong Ilog',
          val: 'bagong_ilog'
        }, {
          text: 'Pinag Buhatan',
          val: 'pinag_buhatan'
        },
        {
          text: 'Bagong Katipunan',
          val: 'bagong_katipunan'
        },
        {
          text: 'Bambang',
          val: 'bambang'
        },
        {
          text: 'Buting',
          val: 'buting'
        },
        {
          text: 'Caniogan',
          val: 'caniogan'
        },
        {
          text: 'Kapitolyo',
          val: 'kapitolyo'
        },
        {
          text: 'Kapasigan',
          val: 'kapasigan'
        },
        {
          text: 'Malinao',
          val: 'malinao'
        },
        {
          text: 'Oranbo',
          val: 'oranbo'
        },
        {
          text: 'Palatiw',
          val: 'palatiw'
        },
        {
          text: 'Pineda',
          val: 'pineda'
        },
        {
          text: 'Sagad',
          val: 'sagad'
        },
        {
          text: 'San Antonio',
          val: 'san_antonio'
        },
        {
          text: 'San Joaquin',
          val: 'san_joaquin'
        },
        {
          text: 'San Jose',
          val: 'san_jose'
        },
        {
          text: 'San Nicolas',
          val: 'san_nicolas'
        },
        {
          text: 'Sta. Cruz',
          val: 'sta_cruz'
        },
        {
          text: 'Sta. Rosa',
          val: 'sta_rosa'
        },
        {
          text: 'Sto. Tomas',
          val: 'sto_tomas'
        },
        {
          text: 'Sumilang',
          val: 'sumilang'
        },
        {
          text: 'Ugong',
          val: 'ugong'
        },
      ]
      var $barangayInput = $('#barangay');
      barangays.forEach((b) => {
        $barangayInput.append('<option value="' + b.val + '">' + b.text + '</option>');
      })
    }
    return {
      init: init
    }
  })(jQuery)
</script>

</body>

</html>