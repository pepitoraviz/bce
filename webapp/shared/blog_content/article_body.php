<?php session_start(); ?>
<article>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto" id="postContentContainer">
      </div>
    </div>
</article>
<hr>
<section>
  <div class="container">
    <h4 class="display-4 my-5">Comments</h4>
    
  </div>
</section>
<?php  if (isset($_SESSION['userId'])) {?>
<section class="py-5">
  <div class="container">
    <form id="commentForm" >
         <div class="form-group">
          <textarea rows="5" class="form-control" name="comment" id="comment" placeholder="Enter your comment"></textarea>
        </div>

        <div class="form-group">
          <button type="submit" class="btn btn-xl btn-success" id="postComment" disabled>Post your comments</button>
        </div>
        <div class="g-recaptcha" data-sitekey="6LeWsF0UAAAAAIaZkHfJl1sB3H73C0JCPX1ei5x6" data-callback="recaptchaCallback"></div>
    </form>
  </div>
</section>
<?php } ?>
<section id="comments" class="py-5">
  <div class="container" id="commentSection">
  </div>
</section>