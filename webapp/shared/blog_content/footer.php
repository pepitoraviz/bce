      <footer class="py-5 bg-black">
        <div class="container">
          <p class="m-0 text-center text-white small">Copyright &copy; BCE Scholarship 2018</p>
        </div>
      <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="/assets/vendors/jquery/jquery.min.js"></script>
    <script src="/assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/vendors/moment/moment.js"></script>

    <!-- Custom scripts for this template -->
    <script src="/assets/js/blog.js"></script>
      <script>
      var blogContent = (function($){
          var postId = "<?php echo $_GET['postId']; ?>";
          var userId = "<?php echo $_SESSION['userId']; ?>";
          var $postContainer = $('#postContentContainer');
          var $commentSection = $('#commentSection');
          var $postTitle = $('#postTitle');
          var $postDate = $('#postDate');
          function init() {
            populateBlogList();
            createComment();
          }
          function createPostListElem (postObj) {
            var el = '<div class="post-preview">'
            el += '<a href="/news_details.php?postId='+postObj._id+'">'
            el += '<h2 class="post-title">'
            el += postObj.title
            el += '</h2>'
            el += '</a>'
            el += '<p class="post-meta">'
            el += 'posted on'+ moment(new Date(postObj.createdAt)).format('ll')
            el += '</p>'
            el += '</div>'
            el += '<hr>'
            return el;
          }
          function populateBlogList(){
            getPostDetails()
              .then((postDetails) => {
                if (postDetails) {
                  $postContainer.append(postDetails.content);
                  $postTitle.html(postDetails.title);
                  $postDate.html(moment( new Date(postDetails.createdAt)).format('ll'));
                }
              })
              getAllComments()
                .then((resp) => {
                    if (Array.isArray(resp) && resp.length > 0){
                      resp.forEach((c) => {
                        var el = createCommentEl(c);
                        $commentSection.append(el);
                      })
                    }

                })
          }

          function createComment () {
            $('#commentForm').on('submit', function(e) {
                e.preventDefault();
                var commentObj = {
                  userId: userId,
                  postId: postId,
                  comment: $('#comment').val()
                }
                sendComment(commentObj)
                  .then((resp) => {
                    window.location.href = 'news_details.php?postId='+postId
                  })
            });
          }

          function createCommentEl(comment) {
              var el = "";
              el += '<div class="media">';
              el += '<div class="media-body">';
              el += '<h4 class="media-heading">'+comment.userId.firstName+' '+comment.userId.lastName+'</h4>';
              el += '<p style="margin:0 !important;">'+comment.comment+'</p>';
               el += '<span style="color:#546E7A;font-size:0.5em;">posted on '+ moment(new Date(comment.createdAt)).format('ll') + '</span>'

              el += '</div>';
              el += '</div>';
              return el;
          }
          function getAllComments() {
            return $.ajax({
              type: 'GET',
              url: 'http://localhost:5000/api/comments/'+postId
            })
          }
          function sendComment (data) {
            return $.ajax({
              type: 'POST',
              url: 'http://localhost:5000/api/comments',
              data: data
            })
          }

          function getPostDetails(){
            return $.ajax({
              method: 'GET',
              url:'http://localhost:5000/api/posts/'+postId
            })
          }
          return {
            init: init
          }
      })(jQuery)

      $(document).ready(() => {
        blogContent.init()
      })
      function recaptchaCallback() {
            $('#postComment').removeAttr('disabled');
      }
    </script>
  </body>

</html>
