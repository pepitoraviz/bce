<script src="assets/vendors/jquery/jquery.min.js"></script>
<script src="assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="/assets/js/blog.js"></script>
<script src="/assets/js/validate.js"></script>
<script src="/assets/js/aval.js"></script>

<script>
    $(document).ready(() => {
        academicApp.init();
    })

    var academicApp = (function ($) {
        var $acadRequirements = $('#acadRequirements'),
            $applicationProgress = $('#applicationProgress'),
            $appLoader = $('#appLoader'),
            $schoolId = $('#schoolId'),
            $birthCert = $('#birthCert'),
            $barangayCertificate = $('#barangayCertificate'),
            $votersId = $('#votersId'),
            $parentCedula = $('#parentCedula'),
            $formOneThreeSeven = $('#formOneThreeSeven'),
            $enrollmentSlip = $('#enrollmentSlip'),
            $essay = $('#essay');

        var userId = "<?php echo $_SESSION['userId']?>";
        var perItemPercentage = 0,
            loaderWidth = 0;

        function init() {
            initializeValidation();
            handleSubmit();
            vadidateGenAve();
        }
         function vadidateGenAve() {
            $('#genAve').on('keydown keyup', function(e){
                var currentValue = String.fromCharCode(e.which);
                var finalValue = $(this).val() + currentValue;
                if(finalValue > 100){
                    e.preventDefault();
                }
            });
        }
        function initializeValidation() {
            $acadRequirements.validate({
                rules: {
                    birthCert: {
                        required: true,
                        extension: "pdf"
                    },
                     genAve: {
                        required: true
                    },
                    schoolId: {
                        required: true,
                        extension: "pdf"
                    },
                    barangayCertificate: {
                        required: true,
                        extension: "pdf"
                    },
                    votersId: {
                        required: true,
                        extension: "pdf"
                    },
                    parentCedula: {
                        required: true,
                        extension: "pdf"
                    },
                    formOneThreeSeven: {
                        required: true,
                        extension: "pdf"
                    },
                    enrollmentSlip: {
                        required: true,
                        extension: "pdf"
                    },
                    essay: {
                        required: true,
                        extension: "pdf"
                    }
                }
            })
        }


        function handleSubmit() {
            $acadRequirements.on('submit', (e) => {
                e.preventDefault();
                let appData = {
                    userId: userId,
                    applicationType: "ACAD",
                    genAve: $('#genAve').val(),
                    yearLvl: "SEC"
                }

                if ($('#acadRequirements').valid()) {
                    var totalFiles = getItemsWithFiles();
                    perItemPercentage = Math.round(100 / totalFiles);
                    $applicationProgress.modal('show');
                    createApplication(appData).then((resp) => {
                        if ('undefined' !== resp.requirementId) {
                            sendReqRes(resp.requirementId)
                                .then(() => {
                                    setTimeout(() => {
                                        $applicationProgress.modal('hide');
                                        window.location.href ='/application.php';
                                    },
                                        2000)
                                })
                        }
                    })
                }
            })
        }

        function updateLoadingPercentage() {
            loaderWidth += perItemPercentage;
            $appLoader.css('width', loaderWidth + '%');
        }

        function getItemsWithFiles() {
            let itemsWithFiles = 0;
            if ($birthCert[0].files[0]) {
                itemsWithFiles++;
            }

             if ($schoolId[0].files[0]) {
                itemsWithFiles++;
            }

            if ($votersId[0].files[0]) {
                itemsWithFiles++;
            }

            if ($parentCedula[0].files[0]) {
                itemsWithFiles++;
            }

            if ($formOneThreeSeven[0].files[0]) {
                itemsWithFiles++;
            }
             if ($essay[0].files[0]) {
                itemsWithFiles++;
            }
            return itemsWithFiles;
        }

        function sendReqRes(reqID) {
            let reqs = [];
            var schoolId = $schoolId[0].files[0];
            if ($birthCert[0].files[0]) {
                reqs.push(createReqData($schoolId[0].files[0], "birthCert", reqID).then(() => {
                    updateLoadingPercentage()
                }));
            }
             if ($schoolId[0].files[0]) {
                reqs.push(createReqData($schoolId[0].files[0], "schoolId", reqID).then(() => {
                    updateLoadingPercentage()
                }));
            }

            if ($votersId[0].files[0]) {
                reqs.push(createReqData($schoolId[0].files[0], "votersId", reqID).then(() =>
                    updateLoadingPercentage()))
            }

            if ($parentCedula[0].files[0]) {
                reqs.push(createReqData($schoolId[0].files[0], "parentCedula", reqID).then(() =>
                    updateLoadingPercentage()))
            }

            if ($formOneThreeSeven[0].files[0]) {
                reqs.push(createReqData($schoolId[0].files[0], "formOneThreeSeven", reqID).then(() =>
                    updateLoadingPercentage()))
            }
            if ($essay[0].files[0]) {
                reqs.push(createReqData($schoolId[0].files[0], "essay", reqID).then(() =>
                    updateLoadingPercentage()))
            }
            return Promise.all(reqs);
        }

        function sendRequirement(opts) {
            return new Promise((resolve, reject) => {
                $.ajax(opts).done(x => resolve(x)).fail(err => reject(err));
            })
        }

        function createReqData(file, reqType, reqId) {
            var formData = new FormData();
            formData.append("requirement", file);
            formData.append("reqId", reqId);
            var opts = {
                type: 'POST',
                cache: false,
                processData: false,
                contentType: false,
                url: 'http://localhost:5000/api/application/' + reqType + '/requirement',
                data: formData
            }
            return sendRequirement(opts);
        }

        function createApplication(data) {
            return $.ajax({
                type: 'POST',
                url: 'http://localhost:5000/api/application/submit',
                data: data
            });
        }

        function sendRequirements(reqData) {
            return $.ajax({
                type: 'POST',
                url: 'http://localhost:5000/api/application/submit',
                cache: false,
                processData: false,
                contentType: false,
                data: reqData
            });
        }
        return {
            init: init
        }
    })(jQuery)
</script>

</body>

</html>