<?php  session_start(); ?>
<?php
    if(isset($_SESSION['userId']) && isset($_SESSION['role'])){
        header("Location: index.php");
        die();
    }
?>
<?php require './shared/register/header.php'; ?>
<div class="container " style="height:100vh;">
    <div class="row h-100">
        <div class="col-md-6 mx-auto my-auto">
            <div class="text-center" style="display: flex; align-items: center; align-content: center;}">
                <a href="index.php"><img src="/assets/img/logo.png" class="mr-3 mb-2" style=" display: inline-block;height: 140px; margin:0"></a>
                <h1 class="text-white text-center display-1" style="display: inline-block;">BCE</h1>

            </div>
            <h2 class="text-white" class="mt-5">Login</h2>
            <form novalidate id="loginForm">
                <div class="card">
                    <div class="card-body">
                         <span class="text-danger" id="invalid" style="display:none;">Invalid username or password</span>
                         <span class="text-danger" id="no-user" style="display:none;">No user found with that email</span>
                         <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" id="email" name="email">
                            </div>

                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="password" id="password">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-warning btn-lg btn-block" style="border-radius:5rem;">Login</button>
                            </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="assets/vendors/jquery/jquery.min.js"></script>
<script src="assets/vendors/bootstrap/js/bootstrap.bundle.min.js"></script>

<script>
    $(document).ready(function(){
        loginApp.init();
    })
    var loginApp = (function($) {
        var $loginForm = $('#loginForm'), $invalid = $('#invalid'), $noUser = $('#no-user');
        function init() {
            onLogin();
        }

        function onLogin(){
            $('#loginForm').on('submit', function(e) {
                e.preventDefault();
                var data = $loginForm.serializeArray().reduce(function(obj, item) {
                  obj[item.name] = item.value;
                  return obj;
              }, {});
              $noUser.hide();
              $invalid.hide();
                login(data)
                    .then((resp) => {
                        if (resp.error) {
                            console.log($invalid);
                            $invalid.show();
                        }
                        if (resp._id) {
                            setSesh(resp)
                                .then(() => {
                                    if ('SCHOLAR' === resp.role) { window.location.href = '/me.php'; }
                                    if ('ADMIN' === resp.role) { window.location.href = '/admin/dashboard.php'; }
                                })
                        }


                    }, (err) => {
                        if(err.status) {
                            $noUser.show();
                        }
                    })
            })
        }
        function login(data){
            return $.ajax({
                method: 'POST',
                url: 'http://localhost:5000/api/users/login',
                data: data
            })
        }
        function setSesh(userObj){
          return $.ajax({
            method: 'POST',
            url: '/functions/set_session.php',
            data: userObj
          });
        }
        return {
            init: init
        }
    })(jQuery)
</script>
</body>

</html>