<?php  session_start(); ?>
<?php
    if(isset($_SESSION['userId']) && isset($_SESSION['role'])){
        header("Location: index.php");
        die();
    }
?>
<?php require './shared/register/header.php'; ?>
<div class="container">
    <div class="row text-center">
        <div class="col-md-12 p-5">
            <h1 class="text-center text-white">New Account Registraion</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9 mx-auto">
            <div class="card">
                <div class="card-body p-5">
                    <form novalidate id="regForm">
                        <h2 class="text-warning">Account Information</h2>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" id="email" name="email">
                        </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password" id="password">
                                </div>
                                <div class="col-md-6">
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control" name="confirm" id="confirm">
                                </div>
                            </div>
                        </div>


                        <h2 class="text-warning">Personal Information</h2>
                        <div class="form-group">
                            <label>Name</label>
                            <div class="row">
                                <div class="col-md-4">
                                    <input type="text" class="form-control" placeholder="First name" name="firstName" id="firstName">
                                </div>

                                <div class="col-md-2">
                                    <input type="text" class="form-control" placeholder="Middle name" name="middleName" id="middleName">
                                </div>

                                <div class="col-md-3">
                                    <input type="text" class="form-control" placeholder="Last name" name="lastName" id="lastName">
                                </div>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" placeholder="Suffix" name="suffix" id="suffix">
                                </div>
                            </div>
                        </div>


                   

                         <div class="form-group">
                            <label>Cellphone Number</label>
                            <input type="text" class="form-control" id="contactNumber" name="contactNumber"> 
                        </div>
                             <div class="form-group">
                            <label>Barangay</label>
                            <select name="barangay" id="barangay" class="form-control">
                                <option value="null" disabled selected>Select Barangay</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <textarea rows="5" class="form-control" name="address" id="address"></textarea>
                        </div>


                        <div class="form-group">
                            <label>School Name</label>
                            <input type="text" class="form-control" name="schoolName" id="schoolName">
                        </div>
                        <div class="form-group">
                            <label>School Address</label>
                            <textarea  rows="5" class="form-control"  name="schoolAddress" id="schoolAddress"></textarea>
                        </div>
                        <div class="form-group p-5">
                            <button type="submit" class="btn btn-warning btn-xl btn-block" style="border-radius:5rem;">Register</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>

<?php require './shared/register/footer.php'; ?>