
<?php require './post_header.php'; ?>
<div class="row ">
  <div class="col-12 p-3 text-right">
        <button class="btn btn-lg btn-success" id="createPostBtn" data-toggle="modal" data-target="#createPostModal">
            Create Post
        </button>
  </div>
</div>
 <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Table Example</div>
        <div class="card-body">
          <div class="table-responsive" >
            <table class="table table-bordered" id="postTbl" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Date</th>
                  <th>Action</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
 </div>
<div class="modal fade" id="createPostModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Post Details</h5>
        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        </div>
        <div class="modal-body">
            <form>
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label" >Title:</label>
                    <input type="text" class="form-control" id="postTitle" placeholder="Enter post title">
                </div>
                <div class="form-group">
                    <label for="message-text" class="col-form-label">Content:</label>
                    <textarea class="form-control" id="post-content"></textarea>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button class="btn btn-success" type="button" id="postBtn">Post</button>
            <button class="btn btn-success" type="button" id="saveBtn">Update</button>
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <!-- <a class="btn btn-primary" href="login.html">Logout</a> -->
        </div>
    </div>
    </div>
</div>
<?php require './post_footer.php'; ?>

<script>
    (function($){
        $(document).ready(function(){
            postList.init();
        })
    })(jQuery)
   var postList = (function($){
       var dtTbl;
        function init(){
            createWYSIWG();
            createDt();
            onSubmit();
            onCreate();
            populateDt();
            onEdit();
        }
        function populateDt(){
              return new Promise((resolve ,reject) => {
                   getAllPosts()
                    .then((resp) => {
                        var currentRow = 0;
                        if(dtTbl && Array.isArray(resp)) {
                            resp.forEach(function(postObj){
                                dtTbl.row.add([postObj.title, moment(new Date(postObj.createdAt)).format('ll'), createEditAction(postObj._id, currentRow)]).draw().node();
                                ++currentRow
                            })
                        }
                        return resolve();
                    }, (err) => {
                        return reject();
                    })
              })
        }
        function onEdit(postObjId){
            $('#postTbl').on('click','.edit-btn', function(){
                var postId = $(this).data('postid');
                var editingRow = $(this).data('row');
                console.log('editingRow: ', editingRow);
                var tempObj = dtTbl.row(editingRow).data();

                var _this = $(this);
                $('#postBtn').hide();
                $('#saveBtn').show();
                getPostContent(postId)
                    .then(function(resp){

                        $('#createPostModal').modal('show');
                        $('#post-content').summernote('editor.pasteHTML', resp.content);
                        $('#postTitle').val(resp.title);
                        $(document).on('click','#saveBtn' ,function(){
                            var postObj = {
                                title: $('#postTitle').val(),
                                content: $('#post-content').val()
                            }
                            // tempObj[0] = postObj.title;
                            // dtTbl.row(editingRow).data(tempObj).invalidate();
                            updatePost(postObj, postId)
                                .then((update) => {
                                    if (update){
                                         tempObj[0] = update.title;
                                        dtTbl.row(editingRow).data(tempObj).invalidate();
                                        $('#createPostModal').modal('hide');
                                    }
                                })
                        })
                    })
            })
        }
        function updatePost(postObj, postObjId){
            return $.ajax({
                method: 'PUT',
                url:'http://localhost:5000/api/posts/'+postObjId,
                data:postObj
            })
        }
        function getPostContent(postObjId){
            return $.ajax({
                type:'GET',
                url:'http://localhost:5000/api/posts/'+postObjId
            })
        }
        function createEditAction(postId ,row){
            var el = '<button class="btn btn-sm btn-warning edit-btn" data-postid="'+postId+'" data-row="'+row+'">Edit</button>';
            return el;
        }
        function getAllPosts(){
            return $.ajax({
                method:'GET',
                url:'http://localhost:5000/api/posts'
            })
        }
        function onSubmit(){
            $('#postBtn').unbind().click(function(){
                let postObj = {
                    content: $('#post-content').val(),
                    title: $('#postTitle').val()
                }
                submitPost(postObj)
                    .then((resp) => {
                        if (resp && dtTbl) {
                            dtTbl.clear().draw();
                            populateDt()
                                .then(function(){
                                    $('#createPostModal').modal('hide');
                                })
                        }

                    })
            })
        }
        function createDt(){
            dtTbl = $('#postTbl').DataTable();
        }
        function submitPost(postObj){
            return $.ajax({
                method: 'POST',
                url:'http://localhost:5000/api/posts',
                data:postObj
            })
        }
        function onCreate(){
            $(document).on('click','#createPostBtn', function(){
                // console.log('wow');
                $('#postBtn').show();
                $('#saveBtn').hide();
            })
        }
        function createWYSIWG(){
            $('#post-content').summernote({
                height: 400,
                placeholder: 'Write here ...',
                focus: false,
                airMode: false,
                fontNames: ['Roboto', 'Calibri', 'Times New Roman', 'Arial'],
                fontNamesIgnoreCheck: ['Roboto', 'Calibri'],
                dialogsInBody: true,
                dialogsFade: true,
                disableDragAndDrop: false,
                toolbar: [
                    // [groupName, [list of button]]
                    ['para', ['style', 'ul', 'ol', 'paragraph']],
                    ['fontsize', ['fontsize']],
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['insert', ['link', 'picture', 'video', 'hr', 'readmore']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['height', ['height']],
                    ['misc', ['undo', 'redo', 'help',]]
                ],
                popover: {
                    air: [
                        ['color', ['color']],
                        ['font', ['bold', 'underline', 'clear']]
                    ]
                },
                print: {
                    //'stylesheetUrl': 'url_of_stylesheet_for_printing'
                }
            });
        }
       return {
           init: init
       }
    })(jQuery)
</script>