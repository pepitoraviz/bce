<?php require './shared/applications/header.php'; ?>
<?php require './shared/applications/wrapper.php'; ?>

<section>
    <div class="row">
        <div class="col-md-10 offset-1 py-5">
            <h2 class="text-dark d-inline-block mb-5">Scholars for interview</h2> &nbsp;
            <button class="btn btn-secondary btn-icon" id="printableList">
                <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Get printable list</button>
            <div class="form-group">
                <div class="row">
                    <div class="col-4 pt-5">
                        <label>Filter </label>
                        <select  id="scheduleFilter" class="form-control">
                            <option value="all">All</option>
                            <option value="today">Today</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-1 py-5">
            <table class="table" id="appTbl">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Year Lvl</th>
                        <th>Scholarship type</th>
                        <th>Barangay</th>
                        <th>Scheduled Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</section>


<div class="modal" tabindex="-1" role="dialog" id="scheduleMdl">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Schedule for interview </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <span id="errMessage"></span>
                <div class="form-group">
                    <label>Schedule date</label>
                    <input id="schedulePicker" class="form-control" />
                </div>
                <div class="form-group">
                    <button class="btn btn-success" id="scheduleBtn">Schedule for an interview</button>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php require './shared/applications/footer.php'; ?>
<script src="/assets/vendors/datatables/jquery.dataTables.js"></script>
<script src="/assets/vendors/datatables/dataTables.bootstrap4.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.dev.js"></script>
<script src="/assets/js/admin_notif.js"></script>
<script src="/assets/vendors/pdf/pdf.js"></script>
<script src="/assets/vendors/pdf/vfs.js"></script>
<script>
    $(document).ready(function () {
        scheds.init();
    })
    var scheds = (function ($) {
        var $appTbl = $('#appTbl'),
            $reqModal = $('#requirements'),
            dtTbl,
            currentApp,
            currentIndex,
            forSched,
            endPoint = $('#scheduleFilter').val() === 'all' ? 'schedules': 'schedules/todate'

        function init() {
            createDt();
            populateTbl();
            scheduleForInterview();
            viewReqs();
            createDatePicker();
            sendSched();
            filterSched();
            getPrintableList();
        }
        function filterSched() {
            $(document).on('change','#scheduleFilter', function(){
                evalEndPoint();
                dtTbl.clear().draw()
                populateTbl();
            })
        }
        function evalEndPoint(){
              endPoint = $('#scheduleFilter').val() === 'all' ? 'schedules': 'schedules/todate'
        }
        function createDatePicker() {
            $('#schedulePicker').datepicker({
                uiLibrary: 'bootstrap4'
            });
        }

        function getPrintableList() {
            $(document).on('click', '#printableList', function(){
                var tblData = [
                ['Name', 'Year Lvl', 'Type','Barangay','Scheduled Date']
            ];
                if (Array.isArray(forSched) && 0 < forSched.length ) {
                    forSched.forEach(function(s) {
                        tblData.push([
                           s.userId.firstName+' '+s.userId.lastName,
                           getYearLvlText(s.applicationId.yearLvl),
                           getScholarTypeText(s.applicationId.applicationType),
                           getBarangay(s.userId.barangay),
                           moment(new Date(s.schedule)).format('ll')
                        ])
                    })
                }
                console.log('tblData: ', tblData);

                var scheduledScholarsReport = {
                    content: [{
                        text: 'Scheduled schoalars for interview as of '+ moment(new Date()).format('ll'),
                        style: 'header'
                    }, {
                        table: {
                            headerRows: 1,
                            widths: ['*', '*', '*','*','*'],
                            body:tblData
                        }
                    }],
                    styles: {
                        header: {
                            fontSize: 18,
                            bold: true,
                            margin: [0, 0, 0, 10]
                        },
                    },
                }

                var schedPdf = pdfMake.createPdf(scheduledScholarsReport);
                schedPdf.download('Scheduled schoalars for interview as of ' + moment(new Date()).format('ll'))
                })
        }

        function createDt() {
            dtTbl = $appTbl.DataTable();
        }

        function createDatePicker() {
            $('#schedulePicker').datepicker({
                uiLibrary: 'bootstrap4'
            });
        }

        function populateTbl() {
            getScheds(endPoint)
                .then((resp) => {
                    if (Array.isArray(resp)) {
                        forSched = resp;
                        resp.forEach((app, ind) => {
                            dtTbl.row.add([
                                    `<span class="applicantName" style="text-decoration:underline;cursor:pointer;" data-appid="${app.applicationId._id}">${app.userId.firstName} ${app.userId.lastName}</span>`,
                                    getYearLvl(app.applicationId.yearLvl),
                                    getScholarshipType(app.applicationId.applicationType),
                                    getBarangay(app.userId.barangay),
                                    moment(app.schedule).format('ll'),
                                    createActions(app)
                                ])
                                .draw();
                        })
                    }
                })
        }

        function createActions(app) {
            var el = '';
            el += '<button class="btn btn-sm btn-success schedInterview" data-appid="' + app.applicationId
                ._id + '">Re schedule interview</button> ';
            return el;
        }

        function getBarangay(barangayVal) {
            var barangays = [{
                    text: 'Bagong Ilog',
                    val: 'bagong_ilog'
                }, , {
                    text: 'Pinag Buhatan',
                    val: 'pinag_buhatan'
                },
                {
                    text: 'Bagong Katipunan',
                    val: 'bagong_katipunan'
                },
                {
                    text: 'Bambang',
                    val: 'bambang'
                },
                {
                    text: 'Buting',
                    val: 'buting'
                },
                {
                    text: 'Caniogan',
                    val: 'caniogan'
                },
                {
                    text: 'Kapitolyo',
                    val: 'kapitolyo'
                },
                {
                    text: 'Kapasigan',
                    val: 'kapasigan'
                },
                {
                    text: 'Malinao',
                    val: 'malinao'
                },
                {
                    text: 'Oranbo',
                    val: 'oranbo'
                },
                {
                    text: 'Palatiw',
                    val: 'palatiw'
                },
                {
                    text: 'Pineda',
                    val: 'pineda'
                },
                {
                    text: 'Sagad',
                    val: 'sagad'
                },
                {
                    text: 'San Antonio',
                    val: 'san_antonio'
                },
                {
                    text: 'San Joaquin',
                    val: 'san_joaquin'
                },
                {
                    text: 'San Jose',
                    val: 'san_jose'
                },
                {
                    text: 'San Nicolas',
                    val: 'san_nicolas'
                },
                {
                    text: 'Sta. Cruz',
                    val: 'sta_cruz'
                },
                {
                    text: 'Sta. Rosa',
                    val: 'sta_rosa'
                },
                {
                    text: 'Sto. Tomas',
                    val: 'sto_tomas'
                },
                {
                    text: 'Sumilang',
                    val: 'sumilang'
                },
                {
                    text: 'Ugong',
                    val: 'ugong'
                },
            ];

            var barangayIndx = barangays.map(b => b.val).indexOf(barangayVal)
            return barangays[barangayIndx].text;
        }


        function viewReqs() {
            $(document).on('click', '.applicantName', function () {
                var app = getApp($(this).data('appid'));
                console.log('app: ', app);
                $('#requirements').modal('toggle');
                var reqs = generateRequirementUrls(app.applicationId.requirementId);
                $('#requirementList').append(reqs);
            })
        }

        function generateRequirementUrls(reqs) {
            var el = ""
            if (reqs) {
                el += '<li><a href="http://localhost:5000/' + reqs.birthCert +
                    '" target="_blank">Birth certificate</a></li>'
                el += '<li><a href="http://localhost:5000/' + reqs.schoolId +
                    '" target="_blank">School Id</a></li>'
                el += '<li><a href="http://localhost:5000/' + reqs.parentCedula +
                    '" target="_blank">Parent Cedula</a></li>'
                el += '<li><a href="http://localhost:5000/' + reqs.formOneThreeSeven +
                    '" target="_blank">Form one three seven</a></li>'
                el += '<li><a href="http://localhost:5000/' + reqs.votersId +
                    '" target="_blank">Voters Id</a></li>'
                el += '<li><a href="http://localhost:5000/' + reqs.essay + '" target="_blank">Essay</a></li>'
            }
            return el;
        }

        function getApp(appId) {
            console.log('appId: ', appId);
            if (Array.isArray(forSched)) {
                let pendingAppIds = forSched.map((app) => {
                    return app.applicationId._id

                }).indexOf(appId)

                return pendingAppIds > -1 ? forSched[pendingAppIds] : null;
            }
        }

        function getScheds(endPoint) {
            return $.ajax({
                type: 'GET',
                url: 'http://localhost:5000/api/'+endPoint
            })
        }

        function approveApplication() {
            $(document).on('click', '.approveapp', function () {
                currentApp = getApp($(this).data('appid'));
                currentIndex = dtTbl.row($(this).parents('tr')).index();

                if (currentApp) {
                    sendApprovalRequest(currentApp._id)
                        .then((resp) => {
                            dtTbl.row(currentIndex).remove().draw()
                        })
                }
            })
        }

        function sendApprovalRequest(appId) {
            return $.ajax({
                type: 'POST',
                url: 'http://localhost:5000/api/applications/approve',
                data: {
                    appId: appId
                }
            })
        }

        function scheduleForInterview() {
            $(document).on('click', '.schedInterview', function () {
                currentApp = getApp($(this).data('appid'));
                currentIndex = dtTbl.row($(this).parents('tr')).index();
                $('#scheduleMdl').modal('toggle');
                $('#errMessage').html('');
                $('#schedulePicker').val(null);
                // var reqs = generateRequirementUrls(app.requirementId);
            })
        }

        function sendScheduleMessage(contactNum, sched) {
            var messageData = {
                numbers: fomatCpNumber(contactNum),
                apiKey: encodeURIComponent('5PI0qbV0y0M-qdjerNWpCGWwiWJkBp892wNBjwqHCK'),
                sender: encodeURIComponent('BCE Sched'),
                message: 'You interview has been rescheduled for you scholarship application on ' + sched
            }
            sendSms(messageData)
        }

        function fomatCpNumber(num) {
            if (-1 === num.substring(0, 2).indexOf('63')) {
                var tempNum = num.split('')
                tempNum[0] = '63'
                return tempNum.join('');
            } else {
                return num
            }
        }


        function sendSms(message) {
            return $.ajax({
                type: 'POST',
                url: 'https://api.txtlocal.com/send/',
                data: message
            })
        }

        function sendSched() {
            $(document).on('click', '#scheduleBtn', function () {
                var scheduleDate = new Date($('#schedulePicker').val())
                var schedObj = {
                    userId: currentApp.userId._id,
                    applicationId: currentApp.applicationId._id,
                    schedule: scheduleDate
                }
                sendForSchedule(schedObj)
                    .then((resp) => {
                        $('#scheduleMdl').modal('toggle');
                        var temp = dtTbl.row(currentIndex).data();
                        currentApp.isScheduled = true;
                        temp[5] = createActions(currentApp);
                        temp[4] = moment(new Date(resp.schedule)).format('ll');
                        dtTbl.row(currentIndex).data(temp).invalidate();

                        sendScheduleMessage(currentApp.userId.contactNumber, moment(resp.schedule).format(
                            'll'));
                    }, (err) => {
                        var errMessage = err.responseJSON['message']
                        if (errMessage) {
                            var explodedMessage = errMessage.split('-');
                            explodedMessage[1] = moment(new Date(explodedMessage[1])).format('ll');
                            $('#errMessage').html(explodedMessage.join(' '));
                        }

                    })
            })
        }

        function sendForSchedule(data) {
            return $.ajax({
                type: 'POST',
                url: 'http://localhost:5000/api/schedules',
                data: data
            })
        }

        function getYearLvl(yearLvl) {
            if ('PRIM' === yearLvl) {
                return '<span class="text-primary">Primary</span>';
            }

            if ('SEC' === yearLvl) {
                return '<span class="text-success">Secondary</span>';
            }

            if ('TERT' === yearLvl) {
                return '<span class="text-danger">Tertiary</span>';
            }
            return '';
        }
        function getYearLvlText(yearLvl) {
             if ('PRIM' === yearLvl) {
                return 'Primary';
            }

            if ('SEC' === yearLvl) {
              return 'Secondary'
            }

            if ('TERT' === yearLvl) {
                return 'Tertiary';
            }
            return '';
        }
        function getScholarTypeText(applicationType){
            if ('ACAD' === applicationType) {
              return 'Academic';
            }

            if ('SPORT' === applicationType) {
               return 'Sports';
            }

            if ('OSY' === applicationType) {
                return 'Out of school youth'
            }
            return '';
        }
        function getScholarshipType(applicationType) {
            if ('ACAD' === applicationType) {
                return '<span class="text-primary">Academic</span>';
            }

            if ('SPORT' === applicationType) {
                return '<span class="text-danger">Sports</span>';
            }

            if ('OSY' === applicationType) {
                return '<span class="text-success">Out of school youth</span>';
            }
            return '';
        }
        return {
            init: init
        }
    })(jQuery);
</script>

<div class="modal" tabindex="-1" role="dialog" id="requirements">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Requirements </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="list-unstyled" id="requirementList">
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>