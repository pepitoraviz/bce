<?php require './shared/applications/header.php'; ?>
<?php require './shared/applications/wrapper.php'; ?>

<section>
    <div class="row">
        <div class="col-md-10 offset-1 py-5">
            <h2>Dashboard</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-1 ">
            <h2 class="text-dark d-inline-block mb-5">Summary</h2> &nbsp;
            <button class="btn btn-secondary btn-icon" id="summaryBtn">
                <i class="fa fa-file-pdf-o" aria-hidden="true"></i> View Report</button>
            <div class="row">
                <div class="col">
                    <div class="card bg-success">
                        <div class="card-body">
                            <span class="text-light">Academic Scholar</span>
                            <h4 class="text-light" id="acadScholarNum"></h4>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card bg-primary">
                        <div class="card-body">
                            <span class="text-light ">Sports Scholar</span>
                            <h4 class="text-light" id="sportScholarNum"></h4>

                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card bg-warning">
                        <div class="card-body">
                            <span class="text-dark">OSY Scholar</span>
                            <h4 class="text-light" id="osyScholarNum"></h4>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="mt-5">
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-1 py-5">
            <div class="row">
                <div class="col-6">
                    <h2 class="text-dark d-inline-block mb-5">Scholars per barangay</h2> &nbsp;
                    <button class="btn btn-secondary btn-icon" id="perBarangayBtn">
                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i> View Report</button>
                    <div class="card">
                        <div class="card-body">
                            <table class="table" id="perBarangayTbl">
                                <thead>
                                    <tr>
                                        <th>Barangay</th>
                                        <th>Total Scholar</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <h2 class="mb-5">School year settings</h2>
                    <div class="card">
                        <div class="card-body">
                            <div class="form-group">
                                <label>School Year</label>
                                <select id="schoolYear" class="form-control"></select>
                            </div>

                            <div class="form-group">
                                <label>Semester Year</label>
                                <select id="semester" class="form-control" id="semester">
                                    <option value="first">First</option>
                                    <option value="second">Second</option>
                                </select>
                            </div>
                            <div class="form-group text-right">
                                <button class="btn btn-success" style="display:none;" id="updateSettingsBtn">Update Settings</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-1 py-5">
            <div class="row">
                <div class="col-lg-6">
                    <h2 class="text-dark d-inline-block mb-5">Rankings</h2> &nbsp;
                    <button class="btn btn-secondary btn-icon" id="rankingsBtn">
                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i> View Report</button>
                </div>
                <div class="col-lg-6">
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-2 col-form-label">Year Level</label>
                        <div class="col-sm-10">
                            <select id="yearLvlOpt" class="form-control">
                                <option value="PRIM">Primary</option>
                                <option value="SEC">Secondary</option>
                                <option value="TERT">Tertiary</option>
                            </select>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-1">
            <div class="row">
                <div class="col py-2">
                    <div class="card">
                        <div class="card-body" id="acadCol">
                            <h2>Academic</h2>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>School Name</th>
                                        <th>Gen Ave</th>
                                    </tr>
                                </thead>
                                <tbody id="acadRanking"></tbody>

                            </table>
                        </div>
                    </div>
                </div>
                <div class="col py-2">
                    <div class="card">
                        <div class="card-body" id="sportCol">
                            <h2>Sports</h2>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>School Name</th>
                                        <th>Gen Ave</th>
                                    </tr>
                                </thead>
                                <tbody id="sportsRanking"></tbody>

                            </table>
                        </div>
                    </div>
                </div>
                <div class="col py-2">
                    <div class="card">
                        <div class="card-body" id="osyCol">
                            <h2>OSY</h2>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>School Name</th>
                                        <th>Gen Ave</th>
                                    </tr>
                                </thead>
                                <tbody id="osyRanking"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="mt-5">

        </div>
    </div>


    <div class="row">
        <div class="col-md-10 offset-1 py-5 ">
            <div class="row">
                <div class="col-12">
                    <h2 class="text-dark d-inline-block mb-5">Monthly Application Report</h2> &nbsp;
                    <button class="btn btn-secondary btn-icon" id="monthlyReport">
                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i> View Report</button>
                    <div class="card">
                        <div class="card-body p-4">
                            <div id="thisMonthReport" style="width:100%;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="mt-5">

        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-1 py-5">
            <div class="row">
                <div class="col-12">
                    <h2 class="text-dark d-inline-block mb-5">Application Breakdown Report</h2> &nbsp;
                    <button class="btn btn-secondary btn-icon" id="breakDownBtn">
                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i> View Report</button>
                    <div class="card">
                        <div class="card-body">
                            <div id="appBreakDown" style="width:100%;height:250px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php require './shared/applications/footer.php'; ?>
<script src="/assets/vendors/datatables/jquery.dataTables.js"></script>
<script src="/assets/vendors/datatables/dataTables.bootstrap4.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.dev.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="/assets/vendors/moment/moment.js"></script>
<script src="/assets/vendors/pdf/pdf.js"></script>
<script src="/assets/vendors/pdf/vfs.js"></script>
<script src="/assets/js/admin_notif.js"></script>
<script src="/assets/js/dashboard_report.js"></script>

<script>
    $(document).ready(function () {
        settings.init();
    })

    var settings = (function ($) {
        var $schoolYear = $('#schoolYear'),
            $semester = $('#semester');
        var currentSy = null;

        function init() {
            createYearOpts();
            showUpdate();
            currentSy = $('#schoolYear').val();
            currentSem = $('#semester').val();
            populateSettings();
            onUpdate();
        }

        function createYearOpts() {
            var min = new Date().getFullYear();
            var max = min + 20;
            for (var i = min; i < max; i++) {
                var opt = document.createElement('option');
                var current = i;
                var next = 1 + i;
                opt.value = current + '-' + next;
                opt.innerHTML = current + '-' + next;
                $schoolYear.append(opt);
            }
            $schoolYear[0].selectedIndex = 0;
        }

        function showUpdate() {
            $(document).on('change', '#schoolYear', function () {
                if ($(this).val() != currentSy) {
                    $('#updateSettingsBtn').show();
                }
                if ($(this).val() == currentSy && currentSem == $('#semester').val()) {
                    $('#updateSettingsBtn').hide();

                }
            })
            $(document).on('change', '#semester', function () {
                if ($(this).val() != currentSem) {
                    $('#updateSettingsBtn').show();
                }
                if ($(this).val() == currentSy && currentSem == $('#semester').val()) {
                    $('#updateSettingsBtn').hide();

                }
            })
        }

        function getCurrentSettings() {
            return $.ajax({
                type: 'GET',
                url: 'http://localhost:5000/api/settings'
            })
        }

        function populateSettings() {
            getCurrentSettings()
                .then(function (resp) {
                    if (resp.schoolYear) {
                        currentSy = resp.schoolYear;
                        $schoolYear.val(resp.schoolYear);
                    }

                    if (resp.semester) {
                        currentSem = resp.semester;
                        $('#semester').val(resp.semester);
                    }
                })
        }

        function onUpdate() {
            $(document).on('click', '#updateSettingsBtn', function () {
                var settingsObj = {
                    schoolYear: $schoolYear.val(),
                    semester: $semester.val()
                }

                updateSettings(settingsObj)
                    .then(function (resp) {
                        if (resp.schoolYear) {
                            currentSy = resp.schoolYear;
                            $schoolYear.val(resp.schoolYear);
                        }

                        if (resp.semester) {
                            currentSem = resp.semester;
                            $('#semester').val(resp.semester);
                        }
                        $('#updateSettingsBtn').hide();

                    })
            })
        }

        function updateSettings(payload) {
            return $.ajax({
                type: 'PUT',
                url: 'http://localhost:5000/api/settings',
                data: payload
            })
        }
        return {
            init: init
        }
    }(jQuery))
</script>