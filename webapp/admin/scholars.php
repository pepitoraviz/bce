<?php require './shared/applications/header.php'; ?>
<?php require './shared/applications/wrapper.php'; ?>

<section>
    <div class="row">
        <div class="col-md-10 offset-1 py-5">
            <h2>Scholar Listing</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-1 py-5">
            <table class="table" id="scholarTbl">
                <thead>
                    <tr>
                        <th>Full Name</th>
                        <th>Barangay</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</section>
<?php require './shared/applications/footer.php'; ?>
<script src="/assets/vendors/datatables/jquery.dataTables.js"></script>
<script src="/assets/vendors/datatables/dataTables.bootstrap4.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.dev.js"></script>

<script src="/assets/js/admin_notif.js"></script>
<script>

    $(document).ready(function(){
        scholarManagement.init();
    })
    var scholarManagement = (function($){
        var dtTbl, $scholarTbl = $('#scholarTbl'),
        currentScholar, currentIndex, scholars;
        function init() {
            createDt();
            populateTbl();
            onConfirm();
            onSuspend();
            onUnsuspend();
        }

        function populateTbl () {
             fetchAllScholars()
                .then(function(resp) {
                    if (Array.isArray(resp)) {
                        scholars = resp;
                        resp.forEach(function(scholar) {
                            dtTbl.row.add([
                                scholar.firstName + ' ' + scholar.lastName,
                                getBarangay(scholar.barangay),
                                getStatus(scholar),
                                createAction(scholar)
                            ])
                            .draw()
                        })
                    }
                })
        }
          function getScholar(scholarId){
            if (Array.isArray(scholars)) {
                let scholarIndx = scholars.map((scholar) => {
                    return scholar._id
                }).indexOf(scholarId)

                return scholarIndx > -1 ? scholars[scholarIndx] : null ;
            }
        }
        function onConfirm () {
            $(document).on('click', '.confirm', function() {
                  currentScholar = getScholar($(this).data('scholarid'));
                  currentIndex = dtTbl.row($(this).parents('tr')).index();
                  var payload = { isConfirmed: true};
                  updateStatus(payload, 'confirm')
                    .then(function(resp) {
                        console.log('resp: ', resp);
                        var updatedScholarObj = updateCurrentScholarOnScholarObj(payload);
                        var statusEl = getStatus(updatedScholarObj);
                        var actionEl = createAction(updatedScholarObj);
                        updateTblData(statusEl , actionEl);
                    })
            })
        }

        function updateCurrentScholarOnScholarObj(status) {
            var scholarInd = getScholarIndex(currentScholar._id)
            if ( null !== scholarInd ) {
                var scholar = scholars[scholarInd];
                if (scholar && status) {
                    scholar = Object.assign(scholar, status)
                    return scholar
                }
            }
            return null;
        }
        function updateTblData(statusEl, actionEl) {
            var recordData = dtTbl.row(currentIndex).data();
            recordData[2] = statusEl;
            recordData[3] = actionEl;
            dtTbl.row(currentIndex).data(recordData).invalidate();
        }
        function onSuspend () {
            $(document).on('click', '.suspend', function() {
                  currentScholar = getScholar($(this).data('scholarid'));
                  currentIndex = dtTbl.row($(this).parents('tr')).index();
                  var payload = { isSuspended: true};
                  updateStatus(payload, 'suspend')
                    .then(function(resp) {
                        console.log('resp: ', resp);
                        var updatedScholarObj = updateCurrentScholarOnScholarObj(payload);
                        var statusEl = getStatus(updatedScholarObj);
                        var actionEl = createAction(updatedScholarObj);
                        updateTblData(statusEl , actionEl);
                    })
            })
        }
        function onUnsuspend () {
            $(document).on('click', '.unsuspend', function() {
                  currentScholar = getScholar($(this).data('scholarid'));
                  currentIndex = dtTbl.row($(this).parents('tr')).index();
                  var payload = { isSuspended: false};
                  updateStatus(payload, 'unsuspend')
                    .then(function(resp) {
                        console.log('resp: ', resp);
                        var updatedScholarObj = updateCurrentScholarOnScholarObj(payload);
                        var statusEl = getStatus(updatedScholarObj);
                        var actionEl = createAction(updatedScholarObj);
                        updateTblData(statusEl , actionEl);
                    })
            })
        }

        function getScholarIndex(scholarId) {
                if (Array.isArray(scholars)) {
                let scholarIndx = scholars.map((scholar) => {
                    return scholar._id
                }).indexOf(scholarId)

                return scholarIndx > -1 ? scholarIndx : null
            }
        }
        function updateStatus (payload, action) {
            return $.ajax({
                type: 'PUT',
                url: 'http://localhost:5000/api/users/'+currentScholar._id+'/'+action,
                data: payload
            })
        }
        function createAction(scholar) {
            var actionEl = '';
            if (scholar) {
                if (scholar.isConfirmed && false === scholar.isSuspended) {
                    actionEl += '<button class="btn btn-danger suspend" data-scholarid="'+scholar._id+'">Suspend</button>'
                }

                if(false === scholar.isConfirmed) {
                    actionEl += '<button class="btn btn-success confirm" data-scholarid="'+scholar._id+'">Confirm</button>'
                }

                if (scholar.isConfirmed && scholar.isSuspended) {
                    actionEl += '<button class="btn btn-warning unsuspend" data-scholarid="'+scholar._id+'">Unsuspend</button>'

                }
            }
            return actionEl;
        }
        function getStatus(scholar) {
            var stat = '';
            if (scholar.isConfirmed && !scholar.isSuspended) {
                stat = '<span class="text-success">Confirmed</span>'
            }

            if (!scholar.isConfirmed) {
                 stat = '<span class="text-warning">Unconfirmed</span>'
            }

            if(scholar.isSuspended) {
               stat = '<span class="text-danger">Suspended</span>'
            }
            return stat;
        }
        function fetchAllScholars () {
            return $.ajax({
                type: 'GET',
                url: 'http://localhost:5000/api/users/scholars'
            })
        }
        function createDt(){
                dtTbl = $scholarTbl.DataTable();
        }
           function getBarangay(barangayVal){
            var barangays = [{
                          text: 'Bagong Ilog',
                val: 'bagong_ilog'
            },{
            text: 'Pinag Buhatan',
                val: 'pinag_buhatan'
            },
            {
                text: 'Bagong Katipunan',
                val: 'bagong_katipunan'
            },
            {
                text: 'Bambang',
                val: 'bambang'
            },
            {
                text: 'Buting',
                val: 'buting'
            },
            {
                text: 'Caniogan',
                val: 'caniogan'
            },
            {
                text: 'Kapitolyo',
                val: 'kapitolyo'
            },
            {
                text: 'Kapasigan',
                val: 'kapasigan'
            },
            {
                text: 'Malinao',
                val: 'malinao'
            },
            {
                text: 'Oranbo',
                val: 'oranbo'
            },
            {
                text: 'Palatiw',
                val: 'palatiw'
            },
            {
                text: 'Pineda',
                val: 'pineda'
            },
            {
                text: 'Sagad',
                val: 'sagad'
            },
            {
                text: 'San Antonio',
                val: 'san_antonio'
            },
            {
                text: 'San Joaquin',
                val: 'san_joaquin'
            },
            {
                text: 'San Jose',
                val: 'san_jose'
            },
            {
                text: 'San Nicolas',
                val: 'san_nicolas'
            },
            {
                text: 'Sta. Cruz',
                val: 'sta_cruz'
            },
            {
                text: 'Sta. Rosa',
                val: 'sta_rosa'
            },
            {
                text: 'Sto. Tomas',
                val: 'sto_tomas'
            },
            {
                text: 'Sumilang',
                val: 'sumilang'
            },
            {
                text: 'Ugong',
                val: 'ugong'
            },
            ];

            var barangayIndx = barangays.map(b => b.val).indexOf(barangayVal)
            return 'undefined' !== typeof barangays[barangayIndx] ?  barangays[barangayIndx]['text'] : ''
        }
        return {
            init: init
        }
    })(jQuery)
</script>

<div class="modal" tabindex="-1" role="dialog" id="requirements">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Requirements  </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <ul class="list-unstyled" id="requirementList">
          </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

