<?php require './shared/applications/header.php'; ?>
<?php require './shared/applications/wrapper.php'; ?>

<section>
    <div class="row">
        <div class="col-md-10 offset-1 py-5">
            <h2>Notifications</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-1 ">
           <div class="list-group py-2" id="notifGroup"></div>
        </div>
    </div>
</section>
<?php require './shared/applications/footer.php'; ?>
<script src="/assets/vendors/datatables/jquery.dataTables.js"></script>
<script src="/assets/vendors/datatables/dataTables.bootstrap4.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.dev.js"></script>

<script src="/assets/js/admin_notif.js"></script>

<script>
    $(document).ready(function() {
        myNotifs.init();
    })
    var myNotifs = (function($) {
        $notifGroup = $('#notifGroup');
        function init() {
            listNotifs();
            readAllNotif();
        }
        function listNotifs() {
            getAllNotifs()
                .then((resp) => {
                    if (Array.isArray(resp)) {
                        resp.forEach((notif) => {
                            var notifEl = generateEl(notif);
                            $notifGroup.append(notifEl);
                        })
                    }
                })
        }
        function generateEl(notif) {
            var el = "";
            el += '<li class="list-group-item list-group-item-action flex-column align-items-start ">'
            el += '<div class="d-flex w-100 justify-content-between">'
            el += '<h5 class="mb-1">'+notif.title+'</h5>'
            el += '<small>'+moment(notif.createdAt).fromNow();+'</small>'
            el += '</div>'
            el += '<p class="mb-1">'+notif.content+'</p>'
            el += '</li>'
            return el;
        }
        function getAllNotifs() {
            return $.ajax({
                type: 'GET',
                url: 'http://localhost:5000/api/admin_notif'
            })
        }
        function readAllNotif(){
            return $.ajax({
                        type: 'GET',
                        url: 'http://localhost:5000/api/admin_notif/readall'
            })
        }
        return {
            init: init
        }
    })(jQuery)
</script>

