<?php require './shared/applications/header.php'; ?>
<?php require './shared/applications/wrapper.php'; ?>

<section>
    <div class="row">
        <div class="col-md-8 offset-2">
            <h2>Scholar Applications</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 offset-1 py-5">
            <table class="table" id="appTbl">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Year Lvl</th>
                        <th>Scholarship type</th>
                        <th>Barangay</th>
                        <th>Date Submited</th>
                        <th>Actions</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</section>
<?php require './shared/applications/footer.php'; ?>
<script src="/assets/vendors/datatables/jquery.dataTables.js"></script>
<script src="/assets/vendors/datatables/dataTables.bootstrap4.js"></script>
<script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.dev.js"></script>
<script src="/assets/js/admin_notif.js"></script>
<script>
    $(document).ready(function(){
        applications.init();
    })
    var applications = (function($) {
        var $appTbl = $('#appTbl'),
        $reqModal = $('#requirements'),
        dtTbl,
        currentApp,
        currentIndex,
        pendingApps;
        function init() {
            createDatePicker();
            createDt();
            populateTbl();
            viewReqs();
            scheduleForInterview();
            sendSched();
            approveApplication();
            $('#scheduleMdl').on('hidden', function () {
               $('#errMessage').html(null);
            })
        }
        function getBarangay(barangayVal){
            var barangays = [
            {
                        text: 'Bagong Ilog',
                val: 'bagong_ilog'
            },{
            text: 'Pinag Buhatan',
                val: 'pinag_buhatan'
            },
            {
                text: 'Bagong Katipunan',
                val: 'bagong_katipunan'
            },
            {
                text: 'Bambang',
                val: 'bambang'
            },
            {
                text: 'Buting',
                val: 'buting'
            },
            {
                text: 'Caniogan',
                val: 'caniogan'
            },
            {
                text: 'Kapitolyo',
                val: 'kapitolyo'
            },
            {
                text: 'Kapasigan',
                val: 'kapasigan'
            },
            {
                text: 'Malinao',
                val: 'malinao'
            },
            {
                text: 'Oranbo',
                val: 'oranbo'
            },
            {
                text: 'Palatiw',
                val: 'palatiw'
            },
            {
                text: 'Pineda',
                val: 'pineda'
            },
            {
                text: 'Sagad',
                val: 'sagad'
            },
            {
                text: 'San Antonio',
                val: 'san_antonio'
            },
            {
                text: 'San Joaquin',
                val: 'san_joaquin'
            },
            {
                text: 'San Jose',
                val: 'san_jose'
            },
            {
                text: 'San Nicolas',
                val: 'san_nicolas'
            },
            {
                text: 'Sta. Cruz',
                val: 'sta_cruz'
            },
            {
                text: 'Sta. Rosa',
                val: 'sta_rosa'
            },
            {
                text: 'Sto. Tomas',
                val: 'sto_tomas'
            },
            {
                text: 'Sumilang',
                val: 'sumilang'
            },
            {
                text: 'Ugong',
                val: 'ugong'
            },
            ];

            var barangayIndx = barangays.map(b => b.val).indexOf(barangayVal)
            return barangays[barangayIndx].text;
        }
        function createDt(){
                dtTbl = $appTbl.DataTable({
                    columns:  [
                        { "width": "25%" },
                        { "width": "10%" },
                        { "width": "15%" },
                        { "width": "10%" },
                        { "width": "10%" },
                        { "width": "30%" },
                    ]
                });
        }
        function createDatePicker() {
            $('#schedulePicker').datepicker({
                uiLibrary: 'bootstrap4'
            });
        }
        function populateTbl() {
                fetchApplications()
                    .then((resp) => {
                        if (Array.isArray(resp)) {
                            pendingApps = resp;
                            resp.forEach((app, ind) => {
                                dtTbl.row.add([
                                    `${app.userId.firstName} ${app.userId.lastName}`,
                                    getYearLvl(app.yearLvl),
                                    getScholarshipType(app.applicationType),
                                    getBarangay(app.userId.barangay),
                                    moment(app.schedule).format('ll'),
                                    createActions(app)
                                ])
                                .draw();
                            })
                        }
                    })
        }

        function createActions(app){
            var el = '';

            el += '<button class="btn btn-sm btn-success view-requirement" data-appid="'+app._id+'" >Requirements</button> '
            el += !app.isScheduled ? '<button class="btn btn-sm btn-success schedInterview" data-appid="'+app._id+'">Schedule interview</button> ' : ' '
            el += ' <button class="btn btn-sm btn-success approveapp" data-appid="'+app._id+'" >Approve</button>'
            return el;
        }

        function viewReqs() {
            $(document).on('click', '.view-requirement', function(){
                var app = getApp($(this).data('appid'));
                $('#requirements').modal('toggle');
                var reqs = generateRequirementUrls(app.requirementId);
                $('#requirementList').append(reqs);
            })
        }

        function generateRequirementUrls(reqs){
            var el= ""
            if (reqs) {
                el += '<li><a href="http://localhost:5000/'+reqs.birthCert+'" target="_blank">Birth certificate</a></li>'
                el += '<li><a href="http://localhost:5000/'+reqs.schoolId+'" target="_blank">School Id</a></li>'
                el += '<li><a href="http://localhost:5000/'+reqs.parentCedula+'" target="_blank">Parent Cedula</a></li>'
                el += '<li><a href="http://localhost:5000/'+reqs.formOneThreeSeven+'" target="_blank">Form one three seven</a></li>'
                el += '<li><a href="http://localhost:5000/'+reqs.votersId+'" target="_blank">Voters Id</a></li>'
                el += '<li><a href="http://localhost:5000/'+reqs.essay+'" target="_blank">Essay</a></li>'
            }
            return el;
        }

        function getApp(appId){
            if (Array.isArray(pendingApps)) {
                let pendingAppIds = pendingApps.map((app) =>  app._id).indexOf(appId);
                return pendingAppIds > -1 ? pendingApps[pendingAppIds] : null ;
            }
        }
        function fetchApplications (){
            return $.ajax({
                type: 'GET',
                url: 'http://localhost:5000/api/applications'
            })
        }
        function approveApplication () {
            $(document).on('click', '.approveapp', function()  {
                  currentApp = getApp($(this).data('appid'));
                  currentIndex = dtTbl.row($(this).parents('tr')).index();

                  if (currentApp) {
                      sendApprovalRequest(currentApp._id)
                        .then((resp) => {
                            dtTbl.row(currentIndex).remove().draw()
                        })
                  }
            })
        }
        function sendApprovalRequest (appId) {
            return $.ajax({
                type: 'POST',
                url: 'http://localhost:5000/api/applications/approve',
                data: { appId: appId }
            })
        }
        function scheduleForInterview(){
            $(document).on('click', '.schedInterview', function(){
                currentApp = getApp($(this).data('appid'));
                currentIndex = dtTbl.row($(this).parents('tr')).index();
                $('#scheduleMdl').modal('toggle');
                $('#errMessage').html('');
                $('#schedulePicker').val(null);
            })
        }

        function sendScheduleMessage(contactNum ,sched) {
            var messageData = {
                numbers: fomatCpNumber(contactNum),
                apiKey: encodeURIComponent('5PI0qbV0y0M-qdjerNWpCGWwiWJkBp892wNBjwqHCK'),
                sender: encodeURIComponent('BCE Sched'),
                message: 'You have successfuly scheduled for an interview for you scholarship application on '+sched
            }
            sendSms(messageData)
        }

        function fomatCpNumber(num){
            if ( -1 === num.substring(0,2).indexOf('63')){
                    var tempNum = num.split('')
                tempNum[0] = '63'
                return tempNum.join('');
            }else{
                return num
            }
        }


        function sendSms(message) {
            return $.ajax({
                type: 'POST',
                url: 'https://api.txtlocal.com/send/',
                data: message
            })
        }
        function sendSched() {
            $(document).on('click', '#scheduleBtn', function(){
                var scheduleDate = new Date($('#schedulePicker').val())
                var schedObj = {
                    userId: currentApp.userId._id,
                    applicationId: currentApp._id,
                    schedule: scheduleDate
                }

                sendForSchedule(schedObj)
                    .then((resp) => {
                        $('#scheduleMdl').modal('toggle');
                            var temp = dtTbl.row(currentIndex).data();
                            currentApp.isScheduled = true;
                            temp[5] = createActions(currentApp);
                            dtTbl.row(currentIndex).data(temp).invalidate();

                            sendScheduleMessage(currentApp.userId.contactNumber , moment(resp.schedule).format('ll'));
                    }, (err) => {
                        var errMessage = err.responseJSON['message']
                        if (errMessage) {
                            var explodedMessage = errMessage.split('-');
                            explodedMessage[1] = moment(new Date(explodedMessage[1])).format('ll');
                            $('#errMessage').html(explodedMessage.join(' '));
                        }

                    })
            })
        }
        function sendForSchedule (data) {
            return $.ajax({
                type: 'POST',
                url: 'http://localhost:5000/api/schedules',
                data: data
            })
        }
        function getYearLvl(yearLvl){
            if ('PRIM' === yearLvl) {
                return '<span class="text-primary">Primary</span>';
            }

            if ('SEC' === yearLvl) {
                return '<span class="text-success">Secondary</span>';
            }

            if ('TERT' === yearLvl) {
                return '<span class="text-danger">Tertiary</span>';
            }
            return '';
        }

        function getScholarshipType(applicationType){
            console.log('applicationType: ', applicationType);
            if ('ACAD' === applicationType) {
                return '<span class="text-primary">Academic</span>';
            }

            if ('SPORT' === applicationType) {
                return '<span class="text-danger">Sports</span>';
            }

            if ('OSY' === applicationType) {
                return '<span class="text-success">Out of school youth</span>';
            }
            return '';
        }
        return {
            init: init
        }
    })(jQuery);
</script>

<div class="modal" tabindex="-1" role="dialog" id="requirements">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Requirements  </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <ul class="list-unstyled" id="requirementList">
          </ul>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="scheduleMdl">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Schedule for interview </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
           <span id="errMessage" ></span>
           <div class="form-group">
                <label>Schedule date</label>
                <input id="schedulePicker" class="form-control" />
           </div>
           <div class="form-group">
            <button class="btn btn-success" id="scheduleBtn">Schedule for an interview</button>
           </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>