<?php  session_start(); ?>
<?php
    if(!isset($_SESSION['userId']) || !isset($_SESSION['role']) || 'ADMIN' != $_SESSION['role']){
        header("Location: /index.php");
        die();
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>BCE - Admin Portal</title>
  <link href="/assets/vendors/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous"> -->
  <link href="/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="/assets/vendors/datatables/dataTables.bootstrap4.css" rel="stylesheet">
   <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
  <link href="/assets/css/editor.css" rel="stylesheet">
  <link href="/assets/css/admin.css" rel="stylesheet">
</head>
<body class="fixed-nav sticky-footer bg-dark" id="page-top">