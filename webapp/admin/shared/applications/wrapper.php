  <nav class="navbar navbar-expand-lg navbar-dark bg-main-color fixed-top" id="mainNav">
    <a class="navbar-brand" href="/admin/dashboard.php">BCE Admin</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <?php require 'sidenav.php'; ?>
        <?php require 'navbar.php'; ?>
    </div>
  </nav>
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">