<?php
session_start();
if ($_POST) {
    if(isset($_POST['_id'])) { setId(); }
    if(isset($_POST['role'])) { setRole(); }
    sendResponse();
}


function setId(){
   $_SESSION['userId'] = $_POST['_id'];
}

function setRole() {
   $_SESSION['role'] = $_POST['role'];
}

function sendResponse() {
    header('Content-type: application/json');
    $response = array(
        'session_stated' => true,
    );

    echo json_encode($response);
}