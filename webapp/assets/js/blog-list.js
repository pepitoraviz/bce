(function($){
    "use strict";
    var $monthItem = $('#month-item');

    // $monthItem.on('click', function() {

    // })
    $('.archive-container').on('click', '.year-label', function(){
        $('.year-icon').toggleClass('fa-chevron-right');
        $('.year-icon').toggleClass('fa-chevron-down');
        $(this).closest('ul').find('.month-list').toggleClass('d-none')
    })
     $('.archive-container').on('click', '.month-label', function(){
        $('.month-icon',this).toggleClass('fa-chevron-right');
        $('.month-icon', this).toggleClass('fa-chevron-down');
        $(this).closest('ul').find('.article-list').toggleClass('d-none')
    })
})(jQuery);