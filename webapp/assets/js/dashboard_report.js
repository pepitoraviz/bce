$(document).ready(function () {
    dashApp.init();
})
var dashApp = (function ($) {
    var $acadScholarNum = $('#acadScholarNum'),
        $sportScholarNum = $('#sportScholarNum'),
        $osyScholarNum = $('#osyScholarNum'),
        $acadRanking = $('#acadRanking'),
        $sportsRanking = $('#sportsRanking'),
        $osyRanking = $('#osyRanking'),
        $sportCol = $('#sportCol'),
        $osyCol = $('#osyCol'),
        $acadCol = $('#acadCol'),
        $yearLvlOpt = $('#yearLvlOpt'),
        yearLvl = $yearLvlOpt.val(),
        $summaryBtn = $('#summaryBtn'),
        $rankingsBtn = $('#rankingsBtn'),
        $monthlyReport = $('#monthlyReport'),
        $breakDownBtn = $('#breakDownBtn');
    var acadRankers = null,
        sportRankers = null,
        baragayReportObj  = null,
        osyRankers = null,
        breakDownRep = null,
        monthlyRep = [];

    function init() {
        populateTopLvlReport();
        onYearLvlChanged();
        initGoogleCharts();
        viewSummaryReport();
        viewRankingReport();
        monthlyReport();
        populatePerBarangayReport();
        viewAppplicationBreakDown();
        viewPerBarangayRprt();
    }

    function onYearLvlChanged() {
        $yearLvlOpt.on('change', function () {
            yearLvl = $(this).val();
            populateTopLvlReport();
        })
    }

    function monthlyReport() {
        var d = new Date();
        var monthInd = d.getMonth();
        var months = ["January", "February", "March",
            "April", "May", "June",
            "July", "August", "September",
            "October", "November", "December"
        ]
        var repBody = [
            ['Date', 'Total']
        ];
        $monthlyReport.unbind().click(function () {
            console.log('wow')
            if (0 < monthlyRep.length) {
                monthlyRep.forEach(function (rep) {
                    repBody.push([
                        moment(new Date(rep.date)).format('ll'), rep.result
                    ])
                })
            }

            var monthlyRepObj = {
                content: [{
                    text: 'Monthly Application report for '+months[monthInd],
                    style: 'header'
                }, {
                    table: {
                        headerRows: 1,
                        widths: ['*', '*'],
                        body: repBody
                    }
                }, ],
                styles: {
                    header: {
                        fontSize: 18,
                        bold: true,
                        margin: [0, 0, 0, 10]
                    },
                    subHeader: {
                        fontSize: 16,
                        bold: true,
                        margin: [5, 25, 5, 25]
                    }
                },
            }
             var monthlyRepPdf = pdfMake.createPdf(monthlyRepObj);
            monthlyRepPdf.download('Monthly Report for the month of '+months[monthInd]);
        })
    }

    function viewRankingReport() {
        $rankingsBtn.unbind().click(function () {
            var rankingsReport = {
                content: [{
                        text: 'Ranking Summary Report ' + getYearLvl(yearLvl) + ' Level',
                        style: 'header'
                    },
                    {
                        text: 'Academic Ranking Report',
                        style: 'subHeader',

                    }, {
                        table: {
                            headerRows: 1,
                            widths: ['*', '*', '*'],
                            body: [
                                ['Name', 'Barangay', 'General Average']
                            ],
                        }
                    }, {
                        text: 'Sport Ranking Report',
                        style: 'subHeader',
                        pageBreak: 'before'

                    }, {
                        table: {
                            headerRows: 1,
                            widths: ['*', '*', '*'],
                            body: [
                                ['Name', 'Barangay', 'General Average']
                            ],
                        }
                    }, {
                        text: 'OSY Ranking Report',
                        style: 'subHeader',
                        pageBreak: 'before'

                    }, {
                        table: {
                            headerRows: 1,
                            widths: ['*', '*', '*'],
                            body: [
                                ['Name', 'Barangay', 'General Average']
                            ],
                        }
                    },
                ],
                styles: {
                    header: {
                        fontSize: 18,
                        bold: true,
                        margin: [0, 0, 0, 10]
                    },
                    subHeader: {
                        fontSize: 16,
                        bold: true,
                        margin: [5, 25, 5, 25]
                    }
                },
            }
            if (acadRankers) {
                acadRankers.forEach(function (r) {
                    rankingsReport['content'][4]['table']['body'].push([
                        r.userId.firstName + ' ' + r.userId.lastName, getBarangay(r.userId.barangay), r.genAve
                    ])
                })

            }

            if (sportRankers) {
                sportRankers.forEach(function (r) {
                    rankingsReport['content'][2]['table']['body'].push([
                        r.userId.firstName + ' ' + r.userId.lastName, getBarangay(r.userId.barangay), r.genAve
                    ])
                })

            }


            if (osyRankers) {
                osyRankers.forEach(function (r) {
                    rankingsReport['content'][6]['table']['body'].push([
                        r.userId.firstName + ' ' + r.userId.lastName, getBarangay(r.userId.barangay), r.genAve
                    ])
                })
            }
            var summaryPdf = pdfMake.createPdf(rankingsReport);
            summaryPdf.download('Ranking report '+getYearLvl(yearLvl));
        })
    }

    function getBarangay(barangayVal) {
        var barangays = [{
                text: 'Bagong Ilog',
                val: 'bagong_ilog'
            },{
            text: 'Pinag Buhatan',
                val: 'pinag_buhatan'
            },
            {
                text: 'Bagong Katipunan',
                val: 'bagong_katipunan'
            },
            {
                text: 'Bambang',
                val: 'buting'
            },
            {
                text: 'Buting',
                val: 'caniogan'
            },
            {
                text: 'Caniogan Ilog',
                val: 'kalawaan'
            },
            {
                text: 'Kapitolyo',
                val: 'kapasigan'
            },
            {
                text: 'Malinao',
                val: 'malinao'
            },
            {
                text: 'Oranbo',
                val: 'oranbo'
            },
            {
                text: 'Palatiw',
                val: 'palatiw'
            },
            {
                text: 'Pineda',
                val: 'pineda'
            },
            {
                text: 'Sagad',
                val: 'sagad'
            },
            {
                text: 'San Antonio',
                val: 'san_antonio'
            },
            {
                text: 'San Joaquin',
                val: 'san_joaquin'
            },
            {
                text: 'San Jose',
                val: 'san_jose'
            },
            {
                text: 'San Nicolas',
                val: 'san_nicolas'
            },
            {
                text: 'Sta. Cruz',
                val: 'sta_cruz'
            },
            {
                text: 'Sta. Rosa',
                val: 'sta_rosa'
            },
            {
                text: 'Sto. Tomas',
                val: 'sto_tomas'
            },
            {
                text: 'Sumilang',
                val: 'sumilang'
            },
            {
                text: 'Ugong',
                val: 'ugong'
            },
        ];

        var barangayIndx = barangays.map(b => b.val).indexOf(barangayVal)
        return barangays[barangayIndx].text;
    }
    function viewAppplicationBreakDown() {
        $breakDownBtn.unbind().click(function(){
            var tblData = [
                ['Type', 'Total']
            ];
            if (breakDownRep) {
                console.log('breakDownRep: ', breakDownRep);
                Object.keys(breakDownRep).forEach(function(b) {
                    tblData.push([
                        evalType(b), breakDownRep[b]
                    ])
                })
            }

            var breakDownReport = {
                content: [{
                    text: 'Application breakdown report as of '+ moment(new Date()).format('ll'),
                    style: 'header'
                }, {
                    table: {
                        headerRows: 1,
                        widths: ['*', '*'],
                        body:tblData
                    }
                }],
                styles: {
                    header: {
                        fontSize: 18,
                        bold: true,
                        margin: [0, 0, 0, 10]
                    },
                },
            }

            var breakDownPdf = pdfMake.createPdf(breakDownReport);
            breakDownPdf.download('Application breakdown as of' + moment(new Date()).format('ll'))
        })
    }
    function viewSummaryReport() {
        $summaryBtn.unbind().click(function () {
            var summaryReport = {
                content: [{
                    text: 'Application Summary Report ',
                    style: 'header'
                }, {
                    table: {
                        headerRows: 1,
                        widths: ['*', '*'],
                        body: [
                            ['Type', 'Total']
                        ]
                    }
                }],
                styles: {
                    header: {
                        fontSize: 18,
                        bold: true,
                        margin: [0, 0, 0, 10]
                    },
                },
                defaultStyle: {
                    // alignment: 'justify'
                }
            }
            if (topLvlBreakDown) {
                Object.keys(topLvlBreakDown).forEach(function (key) {
                    summaryReport.content[1]['table']['body'].push([
                        evalTopLvlType(key), topLvlBreakDown[key]
                    ])
                })
            }
            var summaryPdf = pdfMake.createPdf(summaryReport);
            summaryPdf.download('Summary Report as of '+ moment(new Date()).format('ll'));
        })
    }

    function getMonthReport() {
        return $.ajax({
            type: 'GET',
            url: 'http://localhost:5000/api/applications/month/report'
        })
    }

    function initGoogleCharts() {
        google.charts.load("current", {
            packages: ["calendar", "corechart"]
        });
        google.charts.setOnLoadCallback(drawCalChart);
        google.charts.setOnLoadCallback(drawBreakDown);
    }

    function drawCalChart() {
        getMonthReport()
            .then(function (resp) {
                if (Array.isArray(resp) && 0 < resp.length) {
                    monthlyRep = resp;

                    var dataTable = new google.visualization.DataTable();
                    dataTable.addColumn({
                        type: 'date',
                        id: 'Date'
                    });
                    dataTable.addColumn({
                        type: 'number',
                        id: 'Application Number'
                    });
                    var data = [];
                    resp.forEach(function (report) {
                        data.push([new Date(report.date), report.result]);
                    })
                    dataTable.addRows(data);
                    var d = new Date();
                    var monthInd = d.getMonth();
                    var months = ["January", "February", "March",
                        "April", "May", "June",
                        "July", "August", "September",
                        "October", "November", "December"
                    ]
                    var chart = new google.visualization.Calendar(document.getElementById(
                        'thisMonthReport'));

                    var options = {
                        title: months[monthInd] + ' Report',
                        height: 180,
                        stroke: 'blue',
                        strokeOpacity: 0.8,
                        strokeWidth: 2
                    };

                    chart.draw(dataTable, options);
                }

            })



    }

    function drawBreakDown() {
        getBreakDown()
            .then(function (resp) {

                var rawData = [

                    ['Application Type', 'Application Number']
                ];
                if (resp) {
                    breakDownRep = resp;
                    Object.keys(resp).forEach(function (reprt) {
                        rawData.push([evalType(reprt), resp[reprt]]);
                    })

                    var data = google.visualization.arrayToDataTable(rawData);


                    var chart = new google.visualization.PieChart(document.getElementById('appBreakDown'));

                    chart.draw(data);
                }
            })
    }

    function evalType(type) {
        switch (type) {
            case 'primAcad':
                return 'Academic Primary';
            case 'secAcad':
                return 'Academic Secondary';
            case 'tertAcad':
                return 'Academic Tertiary / Voc';
            case 'primSport':
                return 'Sports Primary';
            case 'secSport':
                return 'Sports Secondary';
            case 'tertSport':
                return 'Sports Tertiary / Voc';
            case 'primOSY':
                return 'OSY Primary';
            case 'secOSY':
                return 'OSY Secondary';
            case 'tertOSY':
                return 'OSY Tertiary / Voc';
            default:
                '';
        }
    }

    function evalTopLvlType(type) {
        switch (type) {
            case 'acad':
                return 'Academic';
            case 'sport':
                return 'Sport';
            case 'osy':
                return 'OSY'
        }
    }

    function getYearLvl(lvl) {
        switch (lvl) {
            case 'PRIM':
                return 'Primary'
            case 'SEC':
                return 'Secondary'
            case 'TERT':
                return 'Tertiary'
        }
    }

    function populateTopLvlReport() {
        getBreakDownTopLvl()
            .then(function (resp) {
                if (resp) {
                    topLvlBreakDown = resp;
                    $acadScholarNum.html(resp.acad);
                    $sportScholarNum.html(resp.sport);
                    $osyScholarNum.html(resp.osy);
                }
            })
        populateRanking();
    }

    function populateRanking() {
        $sportsRanking.html('')
        $acadRanking.html('');
        $osyRanking.html('');
        $acadCol.find('.no-app-message').remove();
        $sportCol.find('.no-app-message').remove();
        $osyCol.find('.no-app-message').remove();
        populateAcadRanking();
        populateSportRanking();
        populateOsyRanking();
    }

    function populateAcadRanking() {
        getRankingByLvl("ACAD")
            .then(function (resp) {
                if (Array.isArray(resp) && 0 < resp.length) {
                    acadRankers = resp;
                    resp.forEach(function (app) {
                        console.log('app: ', app);
                         var el = '<tr>'
                        el += '<td>' + app.user.firstName + ' ' + app.user.lastName +'</td>'
                        el += '<td>' + app.user.schoolName+ '</td>'
                        el += '<td>' + app.genAve + '<td>'
                        el += '<tr>'
                        $acadRanking.append(el);
                    })
                } else {
                    $acadCol.append('<span class="no-app-message">No applications found</span>');
                }
            })
    }

    function populateSportRanking() {
        getRankingByLvl("SPORT")
            .then(function (resp) {
                if (Array.isArray(resp) && 0 < resp.length) {
                    sportRankers = resp;

                    resp.forEach(function (app) {
                       var el = '<tr>'
                        el += '<td>' + app.user.firstName + ' ' + app.user.lastName +'</td>'
                        el += '<td>' + app.user.schoolName+ '</td>';
                        el += '<td>' + app.genAve + '<td>'
                        el += '<tr>'
                        $sportsRanking.append(el);
                    })
                } else {
                    $sportCol.append('<span class="no-app-message">No applications found</span>');
                }
            })
    }

    function populateOsyRanking() {
        getRankingByLvl("OSY")
            .then(function (resp) {
                if (Array.isArray(resp) && 0 < resp.length) {
                    osyRankers = resp;
                    resp.forEach(function (app) {
                        var el = '<tr>'
                        el += '<td>' + app.user.firstName + ' ' + app.user.lastName +'</td>'
                        el += '<td>' + app.user.schoolName+ '</td>'
                        el += '<td>' + app.genAve + '<td>'
                        el += '<tr>'
                        $osyRanking.append(el);
                    })
                } else {
                    $osyCol.append('<span class="no-app-message">No applications found</span>');
                }
            })
    }

    function getBreakDownTopLvl() {
        return $.ajax({
            type: 'GET',
            url: 'http://localhost:5000/api/applications/breakdown_toplvl'
        })
    }

    function getBreakDown() {
        return $.ajax({
            type: 'GET',
            url: 'http://localhost:5000/api/applications/breakdown'
        })
    }
    function getPerBarangayReport() {
        return $.ajax({
            type: 'GET',
            url: 'http://localhost:5000/api/users/barangay_report'
        })
    }
    function populatePerBarangayReport() {
        var $perBarangayDtbl = $('#perBarangayTbl').DataTable();
        getPerBarangayReport()
            .then(function(resp){ 
                if (resp && Array.isArray(resp) && 0 < resp.length) {
                    baragayReportObj = resp;
                    resp.forEach(function(r) {
                        $perBarangayDtbl.row.add([
                            r.text,
                            r.val
                        ])
                        .draw();
                    })
                }
            })
    }

    function viewPerBarangayRprt (){
        $(document).on('click', '#perBarangayBtn', function(){
            if (baragayReportObj) {
                   var tblData = [['Barangay', 'Total']
            ];
            if (baragayReportObj) {
                baragayReportObj.forEach(function(b) {
                    tblData.push([
                       b.text, b.val
                    ])
                })
            }

            var perBarangayReport = {
                content: [{
                    text: 'Per barangay scholars report '+ moment(new Date()).format('ll'),
                    style: 'header'
                }, {
                    table: {
                        headerRows: 1,
                        widths: ['*', '*'],
                        body:tblData
                    }
                }],
                styles: {
                    header: {
                        fontSize: 18,
                        bold: true,
                        margin: [0, 0, 0, 10]
                    },
                },
            }

            var perBarangayPdf = pdfMake.createPdf(perBarangayReport);
            perBarangayPdf.download('Per barangay scholars report as of ' + moment(new Date()).format('ll'))
            }
        })
    }
    function getRankingByLvl(applicationType) {
        return $.ajax({
            type: 'GET',
            url: 'http://localhost:5000/api/applications/' + applicationType + '/' + yearLvl +
                '/getrank'
        })
    }
    return {
        init: init
    }
}(jQuery))