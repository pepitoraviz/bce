var notifApp = (function($){
    var $myNotif = $('#myNotif'), notifLength = 0;
    function init() {
        initSocketConnection();

        if ($myNotif.length > 0 ){
            displayNotifs();
        }
    }
    function displayNotifs() {
        getNotifCount()
            .then((resp) => {
                var $badge = $('#notifCount');
                if ($badge.length > 0) {
                    $badge.remove();
                }
                if (Array.isArray(resp)) {
                    if (resp.length > 0) {
                        notifLength = resp.length;
                        $myNotif.append(' <span class="badge badge-success" id="notifCount">' + notifLength + '</span>')
                    }
                }
            })
    }

    function getNotifCount(){
       return $.ajax({
                type: 'GET',
                url: 'http://localhost:5000/api/student_notifs/'+userId+'/unread'
      })
    }
    function initSocketConnection() {
        socket = io.connect('http://localhost:5000', {
            userId: userId
        });
        socket.on('connect', function (data) {
            socket.emit('register', {
                userId: userId
            });
            askForNotificationPerm()
        });

        socket.on(userId, (payload) => {
            if ('undefined' !== typeof payload.title &&
                'undefined' !== typeof payload.message) {
                showNotif(payload.title, payload.message);
                    showNote();

                if ($myNotif.length > 0) {
                    displayNotifs();
                }
            }
        })

    }

    function showNote () {
        var $statusNote = $('#statusChangeNote');
        if ($statusNote.length) {
            $statusNote.html('');

            $statusNote.html('<span class="text-primary">Status change has been detected please refresh your page to update the current status</span>');
        }
    }
    function showNotif(title, body) {
        notif = new Notification(title, {
            icon: 'https://image.ibb.co/n64tF8/bce.png',
            body: body
        })
    }

    function askForNotificationPerm() {
        if (Notification && !isGranted()) {
            Notification.requestPermission();
        }
    }

    function isGranted() {
        return 'granted' === Notification.permission;
    }
    return  { init: init }
})(jQuery);