var notif = (function($) {
    var socket, notif, notifLength = 0
    $notifHeader = $('#notifHeader'),
    $myNotif = $('#navbarNotif');
    function init() {
        initSocketConnection();
        displayNotif();
    }
    function initSocketConnection() {
        socket = io.connect('http://localhost:5000');
        socket.on('connect', function (data) {
            askForNotificationPerm()
        });

        socket.on('student_profile_update', (payload) => {
            if ('undefined' !== typeof payload.scholarName) {
                showNotif('Scholar profile update', `${payload.scholarName} just updated  his / her profile `);
            }
        })

    }

    function displayNotif() {
        getUnreadNotifs()
            .then((resp) => {
                if (Array.isArray(resp) && resp.length > 0) {
                    notifLength += resp.length;
                    creteNotifBadge();
                    createNotifList(resp);
                }
            })
    }
    function createNotifList(notifs) {
        if (Array.isArray(notifs) && notifs.length > 0) {
            let firstThreeNotifs  = notifs.slice(0, 3);
            firstThreeNotifs.forEach((n) => {
                var el = '';
                el += '<a class="dropdown-item" href="/admin/notifications.php">'
                el += '<span class="text-success">'
                el += '<strong>'
                el += n.title+'</strong>'
                el += '</span>'
                el += '<span class="float-right text-muted" style="font-size:60%;">'+moment(n.createdAt).fromNow()+'</span>'
                el += '<div class="dropdown-message small">'+n.content+'</div>'
                el += '</a>'
                el += '<div class="dropdown-divider"></div>'

                $notifHeader.after(el);
            })
            $notifHeader.after('<div class="dropdown-divider"></div>')
        }
    }
    function creteNotifBadge(){
         $myNotif.append('<span class="badge badge-pill badge-warning">'+notifLength+'</span>')
    }

    function getUnreadNotifs() {
        return $.ajax({
            type: 'GET',
            url: 'http://localhost:5000/api/admin_notif/unread'
        })
    }

    function askForNotificationPerm() {
        if (Notification && !isGranted()) {
            Notification.requestPermission();
        }
    }

    function isGranted() {
        return 'granted' === Notification.permission;
    }

    function showNotif(title, body) {
        notif = new Notification(title, {
            icon: 'https://image.ibb.co/n64tF8/bce.png',
            body: body
        })
    }
    return {
        init: init
    }
})(jQuery)


$(document).ready(function(){
    notif.init();
});
