<?php  session_start(); ?>
<?php
    if(!isset($_SESSION['userId']) || !isset($_SESSION['role']) || 'SCHOLAR' != $_SESSION['role']){
        header("Location: index.php");
        die();
    }
?>
<?php require './shared/me/header.php'; ?>
<?php require './shared/landing_page/navbar.php'; ?>
<?php require './shared/sports/content.php'; ?>
<?php require './shared/sports/footer.php'; ?>

