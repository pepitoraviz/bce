<?php require './shared/landing_page/header.php'; ?>
<?php require './shared/landing_page/hero.php'; ?>
<?php require './shared/landing_page/navbar.php'; ?>




    <section  style="padding:5em 0em">
      <div class="container">
        <div class="row align-items-center">

          <div class="col-lg-12">
            <div >

              <h2 class="display-3">Make your deam posible</h2>
              <p>
                  The BOBBY C. EUSEBIO (BCE) SCHOLARSHIP PROGRAM is one of the key  programs  of the City Government of Pasig. Education enjoys a high priority in the agenda of Mayor Maribel Eusebio’s administration because she shares the vision and belief of her husband,  Former Mayor Bobby C. Eusebio, in the value of  a good education and its crucial role  in achieving  the city’s progress and prosperity. The BCE Scholarship Program is viewed as  a sound investment in the city’s future that will yield handsome returns in the quality of life of  the city’s populace through higher literacy rate and more and  better employment opportunities for the Pasiguenos.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section style="padding:3em 0em">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-12">
            <div >
              <h2 class="display-4 text-center">Scholarship Types</h2>
              <p class="text-center">The BCE Scholarship Program caters to three (3) types of scholars</p>
            </div>
          </div>
          <div class="col-lg-4 text-center">
              <h4 class="diplay-4 ">Academic Scholars</h4>
              <p>
                  Compose the bulk of the scholars .Academic scholarship starts in Grade 5 up to the collegiate level, including tech-voc courses
              </p>
          </div>
          <div class="col-lg-4 text-center">
              <h4 class="diplay-4">Sports Scholars</h4>
              <p>Proficient athletes in their respective sports disciplines, namely archery, athletics, badminton, baseball, chess, football, gymnastics, karatedo, lawn tennis, sepak takraw, sikaran, softball, swimming, table tennis, and taekwondo</p>
          </div>
          <div class="col-lg-4 text-center">
                <h4 class="diplay-4">Former OSY</h4>
                <p>Secondary and tertiary levels are able to return to the  mainstream educational  system  as BCE scholars and regain their future</p>
          </div>
        </div>
      </div>
    </section>

    <section style="padding:4em 0em">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-lg-12">
            <div>
              <h2 class="display-4">We're waiting for you</h2>
              <p>Submit your application form with the essay and all the required supporting documents at the BCE Scholars Office is located at the 2nd floor, Pasig Business Center Bldg. , Pasig City Hall Complex, Pasig City with telephone number 628 3478.  INCOMPLETE APPICATION FORM ( with either incomplete entries or attachments ) shall not be accepted or if inadvertently accepted will not be processed.</p>
            </div>
          </div>
        </div>
      </div>
    </section>

<?php require './shared/landing_page/footer.php'; ?>